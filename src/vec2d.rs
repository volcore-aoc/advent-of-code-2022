#![allow(dead_code)]

use std::cmp::{max, min};
use std::str::FromStr;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct Vec2d {
    pub x: i64,
    pub y: i64,
}

fn err_msg() -> String {
    return "vec2d parse failed".to_string();
}

impl FromStr for Vec2d {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (x, y) = s.split_once(",").ok_or(err_msg())?;

        let x_fromstr = x.parse::<i64>().map_err(|_| err_msg())?;
        let y_fromstr = y.parse::<i64>().map_err(|_| err_msg())?;

        return Ok(Vec2d {
            x: x_fromstr,
            y: y_fromstr,
        });
    }
}

impl Vec2d {
    pub const ZERO: Vec2d = Vec2d { x: 0, y: 0 };
    pub const ONE: Vec2d = Vec2d { x: 1, y: 1 };
    pub const X: Vec2d = Vec2d { x: 1, y: 0 };
    pub const XM: Vec2d = Vec2d { x: -1, y: 0 };
    pub const Y: Vec2d = Vec2d { x: 0, y: 1 };
    pub const YM: Vec2d = Vec2d { x: 0, y: -1 };
    pub const MIN: Vec2d = Vec2d {
        x: i64::MIN,
        y: i64::MIN,
    };
    pub const MAX: Vec2d = Vec2d {
        x: i64::MAX,
        y: i64::MAX,
    };

    pub fn offset(&self, x: i64, y: i64) -> Vec2d {
        return Vec2d {
            x: self.x + x,
            y: self.y + y,
        };
    }

    pub fn add(&self, other: &Vec2d) -> Vec2d {
        return Vec2d {
            x: self.x + other.x,
            y: self.y + other.y,
        };
    }

    pub fn neg(&self) -> Vec2d {
        return Vec2d {
            x: -self.x,
            y: -self.y,
        };
    }

    pub fn sub(&self, other: &Vec2d) -> Vec2d {
        return Vec2d {
            x: self.x - other.x,
            y: self.y - other.y,
        };
    }

    pub fn mul(&self, other: &Vec2d) -> Vec2d {
        return Vec2d {
            x: self.x * other.x,
            y: self.y * other.y,
        };
    }

    pub fn dot(&self, other: &Vec2d) -> i64 {
        return self.x * other.x + self.y * other.y;
    }

    pub fn l0(&self) -> i64 {
        return self.x.abs() + self.y.abs();
    }

    pub fn signum(&self) -> Vec2d {
        return Vec2d {
            x: self.x.signum(),
            y: self.y.signum(),
        };
    }

    pub fn min(&self, other: &Vec2d) -> Vec2d {
        return Vec2d {
            x: min(self.x, other.x),
            y: min(self.y, other.y),
        };
    }

    pub fn max(&self, other: &Vec2d) -> Vec2d {
        return Vec2d {
            x: max(self.x, other.x),
            y: max(self.y, other.y),
        };
    }
}
