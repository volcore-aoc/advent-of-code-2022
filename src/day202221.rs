#[cfg(test)]
mod day202221 {
    use crate::file::{load_and_run_result, Res};
    use std::collections::HashMap;

    #[derive(Debug)]
    struct Ctx {
        monkeys: HashMap<String, Monkey>,
    }

    #[derive(Debug, Clone)]
    struct Monkey {
        value: i64,
        computed: bool,
        grad: f64,
        left: String,
        right: String,
        op: char,
    }

    impl Ctx {
        fn parse(x: &str) -> Res<Ctx> {
            let mut monkeys = HashMap::new();
            for line in x.lines() {
                if line.len() == 0 {
                    continue;
                }
                let (name, rhs) = line.split_once(": ").ok_or("format error 1".to_string())?;
                let parts: Vec<&str> = rhs.split(" ").collect();
                let mut value = 0i64;
                let mut computed = false;
                let mut left = "".to_string();
                let mut right = "".to_string();
                let mut op = ' ';
                if parts.len() == 3 {
                    left = parts[0].to_string();
                    right = parts[2].to_string();
                    op = parts[1]
                        .chars()
                        .next()
                        .ok_or("format error 2".to_string())?;
                } else {
                    value = parts[0].parse::<i64>()?;
                    computed = true;
                }
                monkeys.insert(
                    name.to_string(),
                    Monkey {
                        op,
                        value,
                        left,
                        right,
                        grad: 0.0,
                        computed,
                    },
                );
            }
            return Ok(Ctx { monkeys });
        }

        fn compute(&self, humn_val: Option<i64>) -> Res<HashMap<String, Monkey>> {
            let mut mut_monkeys = self.monkeys.clone();
            match humn_val {
                Some(v) => {
                    mut_monkeys.insert(
                        "humn".into(),
                        Monkey {
                            value: v,
                            op: ' ',
                            grad: 1.0,
                            left: "".into(),
                            right: "".into(),
                            computed: true,
                        },
                    );
                }
                _ => {}
            }
            loop {
                // Check if we're done
                if mut_monkeys["root"].computed {
                    return Ok(mut_monkeys);
                }
                // Loop through the array and update if possible
                let monkeys = mut_monkeys.clone();
                // let mut has_change = false;
                for (_, monkey) in &mut mut_monkeys {
                    if monkey.computed {
                        continue;
                    }
                    let left = &monkeys[&monkey.left];
                    let right = &monkeys[&monkey.right];
                    if !left.computed || !right.computed {
                        continue;
                    }
                    let value = match monkey.op {
                        '+' => left.value + right.value,
                        '-' => left.value - right.value,
                        '*' => left.value * right.value,
                        '/' => left.value / right.value,
                        _ => return Err("Unknown op".into()),
                    };
                    monkey.value = value;
                    monkey.computed = true;
                    monkey.grad = match monkey.op {
                        '+' => left.grad + right.grad,
                        '-' => left.grad - right.grad,
                        '*' => left.grad * right.value as f64 + left.value as f64 * right.grad,
                        '/' => {
                            (left.grad * right.value as f64 - left.value as f64 * right.grad)
                                / (right.value as f64 * right.value as f64)
                        }
                        _ => return Err("unknown op".into()),
                    };
                }
            }
        }

        fn solve_b(&mut self) -> Res<i64> {
            let mut current = 0i64;
            loop {
                let res = self.compute(Some(current))?;
                let left = &res[&res["root"].left];
                let right = &res[&res["root"].right];
                if left.value == right.value {
                    break;
                }
                // Estimate close value
                current = current + ((right.value - left.value) as f64 / left.grad) as i64;
            }
            // Now find the **smallest** value that also fulfils this
            loop {
                let res = self.compute(Some(current - 1))?;
                let left = &res[&res["root"].left];
                let right = &res[&res["root"].right];
                if left.value != right.value {
                    return Ok(current);
                }
                current = current - 1;
            }
        }
    }

    fn solve_a(x: &str) -> Res<i64> {
        let ctx = Ctx::parse(x)?;
        return Ok(ctx.compute(None)?["root"].value);
    }

    fn solve_b(x: &str) -> Res<i64> {
        let mut ctx = Ctx::parse(x)?;
        return Ok(ctx.solve_b()?);
    }

    static TEST_CASE: &str = "root: pppw + sjmn\ndbpl: 5\ncczh: sllz + lgvd\nzczc: 2\nptdq: humn - dvpt\ndvpt: 3\nlfqf: 4\nhumn: 5\nljgn: 2\nsjmn: drzm * dbpl\nsllz: 4\npppw: cczh / lfqf\nlgvd: ljgn * ptdq\ndrzm: hmdt - zczc\nhmdt: 32\n";

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(solve_a(TEST_CASE)?, 152);
        load_and_run_result(21, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b(TEST_CASE)?, 301);
        load_and_run_result(21, 'b', solve_b);
        return Ok(());
    }
}
