#![allow(dead_code)]

use std::cmp::{max, min};

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Ord, PartialOrd)]
pub struct Vec3d {
    pub x: i64,
    pub y: i64,
    pub z: i64,
}

impl Vec3d {
    pub const ZERO: Vec3d = Vec3d { x: 0, y: 0, z: 0 };
    pub const ONE: Vec3d = Vec3d { x: 1, y: 1, z: 1 };
    pub const X: Vec3d = Vec3d { x: 1, y: 0, z: 0 };
    pub const Y: Vec3d = Vec3d { x: 0, y: 1, z: 0 };
    pub const Z: Vec3d = Vec3d { x: 0, y: 0, z: 1 };
    pub const MIN: Vec3d = Vec3d {
        x: i64::MIN,
        y: i64::MIN,
        z: i64::MIN,
    };
    pub const MAX: Vec3d = Vec3d {
        x: i64::MAX,
        y: i64::MAX,
        z: i64::MAX,
    };

    pub fn add(&self, other: &Vec3d) -> Vec3d {
        return Vec3d {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        };
    }

    pub fn neg(&self) -> Vec3d {
        return Vec3d {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        };
    }

    pub fn sub(&self, other: &Vec3d) -> Vec3d {
        return Vec3d {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        };
    }

    pub fn mul(&self, other: &Vec3d) -> Vec3d {
        return Vec3d {
            x: self.x * other.x,
            y: self.y * other.y,
            z: self.z * other.z,
        };
    }

    pub fn dot(&self, other: &Vec3d) -> i64 {
        return self.x * other.x + self.y * other.y + self.z * other.z;
    }

    pub fn l0(&self) -> i64 {
        return self.x.abs() + self.y.abs() + self.z.abs();
    }

    pub fn signum(&self) -> Vec3d {
        return Vec3d {
            x: self.x.signum(),
            y: self.y.signum(),
            z: self.z.signum(),
        };
    }

    pub fn min(&self, other: &Vec3d) -> Vec3d {
        return Vec3d {
            x: min(self.x, other.x),
            y: min(self.y, other.y),
            z: min(self.z, other.z),
        };
    }

    pub fn max(&self, other: &Vec3d) -> Vec3d {
        return Vec3d {
            x: max(self.x, other.x),
            y: max(self.y, other.y),
            z: max(self.z, other.z),
        };
    }
}
