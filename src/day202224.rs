#[cfg(test)]
mod day202224 {
    use crate::file::{load_and_run_result, Res};
    use crate::vec3d::Vec3d;
    use std::cmp::Ordering;
    use std::collections::{BinaryHeap, HashMap};

    #[derive(Debug)]
    struct Ctx {
        r_rows: Vec<Vec<usize>>,
        l_rows: Vec<Vec<usize>>,
        t_cols: Vec<Vec<usize>>,
        b_cols: Vec<Vec<usize>>,
        width: usize,
        height: usize,
    }

    #[derive(Copy, Clone, Eq, PartialEq)]
    struct HeapState {
        cost: i64,
        pos: Vec3d,
    }

    impl Ord for HeapState {
        fn cmp(&self, other: &Self) -> Ordering {
            // Flip around self.cmp(other) as we're interested in the **smallest**
            other
                .cost
                .cmp(&self.cost)
                .then_with(|| self.pos.cmp(&other.pos))
        }
    }

    impl PartialOrd for HeapState {
        fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
            Some(self.cmp(other))
        }
    }

    impl Ctx {
        fn parse(input: &str) -> Res<Ctx> {
            let mut r_rows = Vec::new();
            let mut l_rows = Vec::new();
            let mut t_cols = Vec::new();
            let mut b_cols = Vec::new();
            let mut width = 0;
            let mut height = 0;
            for line in input.lines() {
                if line.len() == 0 || line.starts_with("#.#") || line.starts_with("###") {
                    continue;
                }
                let tline = line.trim_matches(|c| c == '#');
                width = tline.len();
                while t_cols.len() < width {
                    t_cols.push(Vec::new());
                    b_cols.push(Vec::new());
                }
                let mut r_row = Vec::new();
                let mut l_row = Vec::new();
                for (idx, c) in tline.chars().enumerate() {
                    match c {
                        '>' => r_row.push(idx),
                        '<' => l_row.push(idx),
                        '^' => t_cols[idx].push(height),
                        'v' => b_cols[idx].push(height),
                        _ => continue,
                    }
                }
                l_rows.push(l_row);
                r_rows.push(r_row);
                height += 1;
            }
            return Ok(Ctx {
                r_rows,
                l_rows,
                t_cols,
                b_cols,
                width,
                height,
            });
        }

        fn can_move_to(&self, p: Vec3d) -> bool {
            // Always move to target
            if p.x == self.width as i64 - 1 && p.y == self.height as i64 {
                return true;
            }
            if p.x == 0 && p.y == -1 {
                return true;
            }
            // Don't move out of bounds
            if p.x < 0
                || p.x >= self.width as i64
                || p.y < 0
                || p.y >= self.height as i64
                || p.z < 0
            {
                return false;
            }
            // Are we colliding with any blizzards
            for &b in self.r_rows[p.y as usize].as_slice() {
                if p.x == (b as i64 + p.z).rem_euclid(self.width as i64) {
                    return false;
                }
            }
            for &b in self.l_rows[p.y as usize].as_slice() {
                if p.x == (b as i64 - p.z).rem_euclid(self.width as i64) {
                    return false;
                }
            }
            for &b in self.t_cols[p.x as usize].as_slice() {
                if p.y == (b as i64 - p.z).rem_euclid(self.height as i64) {
                    return false;
                }
            }
            for &b in self.b_cols[p.x as usize].as_slice() {
                if p.y == (b as i64 + p.z).rem_euclid(self.height as i64) {
                    return false;
                }
            }
            return true;
        }

        fn target(&self, t: i64) -> Vec3d {
            return Vec3d {
                x: self.width as i64 - 1,
                y: self.height as i64,
                z: t,
            };
        }

        fn start(&self, t: i64) -> Vec3d {
            return Vec3d { x: 0, y: -1, z: t };
        }

        fn heuristic(&self, p: Vec3d, t: Vec3d) -> i64 {
            // Guess the remaining distance to the target. Ignore the temporal part
            return (t.x - p.x).abs() + (t.y - p.y).abs();
        }

        fn find_path(&self, start: Vec3d, target: Vec3d) -> usize {
            let neighbours = vec![
                Vec3d { x: -1, y: 0, z: 1 },
                Vec3d { x: 1, y: 0, z: 1 },
                Vec3d { x: 0, y: -1, z: 1 },
                Vec3d { x: 0, y: 1, z: 1 },
                Vec3d { x: 0, y: 0, z: 1 },
            ];
            let mut heap = BinaryHeap::new();
            heap.push(HeapState {
                pos: start,
                cost: self.heuristic(start, target),
            });
            let mut min_cost: HashMap<Vec3d, i64> = HashMap::new();
            min_cost.insert(start, 0);
            while let Some(HeapState { cost: _, pos }) = heap.pop() {
                // Get the minimum cost for pos
                let cost = min_cost[&pos];
                for n in neighbours.as_slice() {
                    let np = pos.add(n);
                    if !self.can_move_to(np) {
                        continue;
                    }
                    let h = self.heuristic(np, target);
                    if h == 0 {
                        // We have arrived
                        return cost as usize + 1;
                    }
                    // Check if we've already been there cheaper
                    if let Some(&prev_cost) = min_cost.get(&np) {
                        if prev_cost <= cost + 1 {
                            continue;
                        }
                    }
                    // Save current cost as min cost
                    min_cost.insert(np, cost + 1);
                    // Queue to (re)visit
                    let ncost = cost + 1 + h;
                    heap.push(HeapState {
                        cost: ncost,
                        pos: np,
                    });
                }
            }
            return 0;
        }
    }

    fn solve_a(x: &str) -> Res<usize> {
        let ctx = Ctx::parse(x)?;
        return Ok(ctx.find_path(ctx.start(0), ctx.target(0)));
    }

    fn solve_b(x: &str) -> Res<usize> {
        let ctx = Ctx::parse(x)?;
        let cost_a = ctx.find_path(ctx.start(0), ctx.target(0));
        let cost_b = ctx.find_path(ctx.target(cost_a as i64), ctx.start(0));
        let cost_c = ctx.find_path(ctx.start((cost_b + cost_a) as i64), ctx.target(0));
        return Ok(cost_a + cost_b + cost_c);
    }

    static TEST_CASE: &str = "#.######\n#>>.<^<#\n#.<..<<#\n#>v.><>#\n#<^v^^>#\n######.#\n";

    #[test]
    fn test_a() -> Res<()> {
        let ctx = Ctx::parse(TEST_CASE)?;
        assert_eq!(ctx.can_move_to(Vec3d { x: 0, y: 0, z: 0 }), false);
        assert_eq!(ctx.can_move_to(Vec3d { x: 0, y: 0, z: 1 }), true);
        assert_eq!(ctx.can_move_to(Vec3d { x: 0, y: 0, z: 2 }), true);
        assert_eq!(ctx.can_move_to(Vec3d { x: 0, y: 0, z: 3 }), false);
        assert_eq!(ctx.can_move_to(Vec3d { x: 2, y: 1, z: 0 }), true);
        assert_eq!(ctx.can_move_to(Vec3d { x: 2, y: 1, z: 1 }), true);
        assert_eq!(ctx.can_move_to(Vec3d { x: 2, y: 1, z: 2 }), false);
        assert_eq!(ctx.can_move_to(Vec3d { x: 1, y: 3, z: 0 }), false);
        assert_eq!(ctx.can_move_to(Vec3d { x: 1, y: 3, z: 1 }), false);
        assert_eq!(ctx.can_move_to(Vec3d { x: 1, y: 3, z: 2 }), false);
        assert_eq!(ctx.can_move_to(Vec3d { x: 1, y: 3, z: 3 }), true);
        assert_eq!(ctx.can_move_to(Vec3d { x: 4, y: 3, z: 0 }), false);
        assert_eq!(ctx.can_move_to(Vec3d { x: 4, y: 3, z: 1 }), false);
        assert_eq!(ctx.can_move_to(Vec3d { x: 4, y: 3, z: 2 }), false);
        assert_eq!(ctx.can_move_to(Vec3d { x: 4, y: 3, z: 3 }), true);
        assert_eq!(solve_a(TEST_CASE)?, 18);
        load_and_run_result(24, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b(TEST_CASE)?, 54);
        load_and_run_result(24, 'b', solve_b);
        return Ok(());
    }
}
