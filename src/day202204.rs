#[cfg(test)]
mod day202204 {
    use crate::file::{load_and_run_result, Res};

    fn parse_range(x: &str) -> Res<[i64; 2]> {
        let parts: Vec<&str> = x.split("-").collect();
        if parts.len() != 2 {
            return Err("range needs two elements".into());
        }
        let left = parts[0].parse::<i64>()?;
        let right = parts[1].parse::<i64>()?;
        return Ok([left, right]);
    }

    fn parse(x: &str) -> Res<[[i64; 2]; 2]> {
        let parts: Vec<&str> = x.split(",").collect();
        let left = parse_range(parts[0])?;
        let right = parse_range(parts[1])?;
        return Ok([left, right]);
    }

    fn solve_a(x: &str) -> Res<i64> {
        let mut total = 0i64;
        for line in x.lines() {
            let ranges = parse(line)?;
            // Left included in right?
            if ranges[0][0] >= ranges[1][0] && ranges[0][1] <= ranges[1][1] {
                total += 1;
                // Prevent double counting
                continue;
            }
            // Right included in left?
            if ranges[1][0] >= ranges[0][0] && ranges[1][1] <= ranges[0][1] {
                total += 1;
            }
        }
        return Ok(total);
    }

    fn solve_b(x: &str) -> Res<i64> {
        let mut total = 0i64;
        for line in x.lines() {
            let ranges = parse(line)?;
            // high left overlap low right?
            if ranges[0][1] >= ranges[1][0] && ranges[0][0] <= ranges[1][1] {
                total += 1;
                continue;
            }
            // high right overlap low left
            if ranges[1][1] >= ranges[0][0] && ranges[1][0] <= ranges[0][1] {
                total += 1;
            }
        }
        return Ok(total);
    }

    static TEST_CASE: &str = "2-4,6-8\n2-3,4-5\n5-7,7-9\n2-8,3-7\n6-6,4-6\n2-6,4-8\n";

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(solve_a(TEST_CASE)?, 2);
        load_and_run_result(4, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b(TEST_CASE)?, 4);
        load_and_run_result(4, 'b', solve_b);
        return Ok(());
    }
}
