#[cfg(test)]
mod day202203 {
    use crate::file::{load_and_run_result, Res};
    use std::collections::HashSet;

    fn hist(x: &str) -> HashSet<char> {
        let mut set = HashSet::new();
        for c in x.chars() {
            set.insert(c);
        }
        return set;
    }

    fn score(x: char) -> i64 {
        if x >= 'a' && x <= 'z' {
            return x as i64 - 'a' as i64 + 1;
        }
        if x >= 'A' && x <= 'Z' {
            return x as i64 - 'A' as i64 + 27;
        }
        return 0;
    }

    fn solve_a(x: &str) -> Res<i64> {
        let mut total = 0i64;
        for line in x.lines() {
            let mid = line.len() / 2;
            let left = &line[0..mid];
            let right = &line[mid..];
            let left_hist = hist(left);
            let right_hist = hist(right);
            let intersect = left_hist.intersection(&right_hist);
            for c in intersect {
                total += score(c.clone());
            }
        }
        return Ok(total);
    }

    fn solve_b(x: &str) -> Res<i64> {
        let mut total = 0i64;
        let lines: Vec<&str> = x.lines().collect();
        for i in 0..lines.len() / 3 {
            let a_hist = hist(lines[i * 3 + 0]);
            let b_hist = hist(lines[i * 3 + 1]);
            let c_hist = hist(lines[i * 3 + 2]);
            for c in a_hist {
                if !b_hist.contains(&c) {
                    continue;
                }
                if !c_hist.contains(&c) {
                    continue;
                }
                total += score(c);
            }
        }
        return Ok(total);
    }

    static TEST_CASE: &str = "vJrwpWtwJgWrhcsFMMfFFhFp\njqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\nPmmdzqPrVvPwwTWBwg\nwMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\nttgJtRGJQctTZtZT\nCrZsJsPPZsGzwwsLwLmpwMDw\n";

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(solve_a(TEST_CASE)?, 157);
        load_and_run_result(3, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b(TEST_CASE)?, 70);
        load_and_run_result(3, 'b', solve_b);
        return Ok(());
    }
}
