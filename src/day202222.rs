#[cfg(test)]
mod day202222 {
    use crate::file::{load_and_run_result, Res};
    use crate::vec2d::Vec2d;
    use std::cmp::max;

    struct Ctx {
        grid: Vec<Vec<char>>,
        instructions: Vec<Instruction>,
        width: usize,
        height: usize,
        cube_mode: bool,
    }

    enum Instruction {
        Steps(usize),
        RotateLeft,
        RotateRight,
    }

    #[derive(Debug, Copy, Clone, PartialEq)]
    enum Direction {
        Right,
        Down,
        Left,
        Up,
    }

    fn rotate_left(dir: Direction) -> Direction {
        match dir {
            Direction::Left => Direction::Down,
            Direction::Down => Direction::Right,
            Direction::Right => Direction::Up,
            Direction::Up => Direction::Left,
        }
    }

    fn rotate_right(dir: Direction) -> Direction {
        match dir {
            Direction::Left => Direction::Up,
            Direction::Down => Direction::Left,
            Direction::Right => Direction::Down,
            Direction::Up => Direction::Right,
        }
    }

    impl Ctx {
        fn parse(input: &str) -> Res<Ctx> {
            let mut instructions = Vec::new();
            let mut grid = Vec::new();
            let mut width = 0;
            let mut height = 0;
            for line in input.lines() {
                if line.len() == 0 {
                    continue;
                }
                if line.contains(&['R', 'L']) {
                    // Instruction line
                    for part in line.split_inclusive(&['R', 'L']) {
                        let steps = part.trim_end_matches(&['R', 'L']).parse::<usize>()?;
                        instructions.push(Instruction::Steps(steps));
                        if part.ends_with("L") {
                            instructions.push(Instruction::RotateLeft);
                        } else if part.ends_with("R") {
                            instructions.push(Instruction::RotateRight);
                        }
                    }
                    continue;
                }
                // Normal grid line
                width = max(width, line.len());
                grid.push(line.chars().collect());
                height += 1;
            }
            return Ok(Ctx {
                grid,
                instructions,
                width,
                height,
                cube_mode: false,
            });
        }

        fn start(&self) -> (Vec2d, Direction) {
            for (x, &c) in self.grid[0].iter().enumerate() {
                if c != ' ' {
                    return (Vec2d { x: x as i64, y: 0 }, Direction::Right);
                }
            }
            return (Vec2d { x: 0, y: 0 }, Direction::Right);
        }

        fn get(&self, pos: &Vec2d) -> char {
            if pos.y < 0 || pos.y >= self.grid.len() as i64 {
                return ' ';
            }
            let sub = &self.grid[pos.y as usize];
            if pos.x < 0 || pos.x >= sub.len() as i64 {
                return ' ';
            }
            return sub[pos.x as usize];
        }

        fn side_len(&self) -> i64 {
            return match self.width {
                16 => 4,
                150 => 50,
                _ => panic!("unknown side length"),
            };
        }

        fn entry_applies(&self, pos: &Vec2d, dir: &Direction, txy: (i64, i64)) -> (bool, i64) {
            let w = self.side_len();
            let x_start = txy.0 * w;
            let x_end = x_start + w - 1;
            let y_start = txy.1 * w;
            let y_end = y_start + w - 1;
            match dir {
                Direction::Left => {
                    if pos.x != x_start - 1 || pos.y < y_start || pos.y > y_end {
                        return (false, 0);
                    }
                    return (true, pos.y - y_start);
                }
                Direction::Right => {
                    if pos.x != x_end + 1 || pos.y < y_start || pos.y > y_end {
                        return (false, 0);
                    }
                    return (true, pos.y - y_start);
                }
                Direction::Up => {
                    if pos.y != y_start - 1 || pos.x < x_start || pos.x > x_end {
                        return (false, 0);
                    }
                    return (true, pos.x - x_start);
                }
                Direction::Down => {
                    if pos.y != y_end + 1 || pos.x < x_start || pos.x > x_end {
                        return (false, 0);
                    }
                    return (true, pos.x - x_start);
                }
            }
        }

        fn map_to_edge(
            &self,
            delta: i64,
            new_cell: (i64, i64),
            new_dir: &Direction,
            forward: bool,
        ) -> (Vec2d, Direction) {
            let w = self.side_len();
            let x_start = new_cell.0 * w;
            let x_end = x_start + w - 1;
            let y_start = new_cell.1 * w;
            let y_end = y_start + w - 1;
            let pos = if forward { delta } else { w - delta - 1 };
            return (
                match new_dir {
                    Direction::Up => Vec2d {
                        x: x_start + pos,
                        y: y_end,
                    },
                    Direction::Down => Vec2d {
                        x: x_start + pos,
                        y: y_start,
                    },
                    Direction::Left => Vec2d {
                        x: x_end,
                        y: y_start + pos,
                    },
                    Direction::Right => Vec2d {
                        x: x_start,
                        y: y_start + pos,
                    },
                },
                new_dir.clone(),
            );
        }

        fn warp(&self, pos: &Vec2d, dir: &Direction) -> (Vec2d, Direction) {
            if self.cube_mode {
                // Transition table
                let table = if self.side_len() == 4 {
                    [
                        ((2, 0), Direction::Left, (1, 1), Direction::Down, true),
                        ((2, 0), Direction::Up, (0, 1), Direction::Down, false),
                        ((2, 0), Direction::Right, (3, 2), Direction::Left, false),
                        ((0, 1), Direction::Up, (2, 0), Direction::Down, false),
                        ((0, 1), Direction::Left, (3, 2), Direction::Up, false),
                        ((0, 1), Direction::Down, (2, 2), Direction::Up, false),
                        ((1, 1), Direction::Up, (2, 0), Direction::Right, true),
                        ((1, 1), Direction::Down, (2, 2), Direction::Right, false),
                        ((2, 1), Direction::Right, (3, 2), Direction::Down, false),
                        ((2, 2), Direction::Left, (1, 1), Direction::Up, false),
                        ((2, 2), Direction::Down, (0, 1), Direction::Up, false),
                        ((3, 2), Direction::Up, (2, 1), Direction::Left, false),
                        ((3, 2), Direction::Right, (2, 0), Direction::Left, false),
                        ((3, 2), Direction::Down, (0, 1), Direction::Right, false),
                    ]
                } else {
                    [
                        // A=1,0  B=2,0  C=1,1  D=0,2  E=1,2  F=0,3
                        // A
                        ((1, 0), Direction::Up, (0, 3), Direction::Right, true),
                        ((1, 0), Direction::Left, (0, 2), Direction::Right, false),
                        // B
                        ((2, 0), Direction::Up, (0, 3), Direction::Up, true),
                        ((2, 0), Direction::Right, (1, 2), Direction::Left, false),
                        ((2, 0), Direction::Down, (1, 1), Direction::Left, true),
                        // C
                        ((1, 1), Direction::Left, (0, 2), Direction::Down, true),
                        ((1, 1), Direction::Right, (2, 0), Direction::Up, true),
                        // D
                        ((0, 2), Direction::Up, (1, 1), Direction::Right, true),
                        ((0, 2), Direction::Left, (1, 0), Direction::Right, false),
                        // E
                        ((1, 2), Direction::Right, (2, 0), Direction::Left, false),
                        ((1, 2), Direction::Down, (0, 3), Direction::Left, true),
                        // F
                        ((0, 3), Direction::Left, (1, 0), Direction::Down, true),
                        ((0, 3), Direction::Right, (1, 2), Direction::Up, true),
                        ((0, 3), Direction::Down, (2, 0), Direction::Down, true),
                    ]
                };
                for entry in table {
                    if entry.1 != *dir {
                        continue;
                    }
                    let (applies, delta) = self.entry_applies(pos, dir, entry.0);
                    if !applies {
                        continue;
                    }
                    return self.map_to_edge(delta, entry.2, &entry.3, entry.4);
                }
                return (pos.clone(), dir.clone());
            } else {
                return (
                    Vec2d {
                        x: pos.x.rem_euclid(self.width as i64),
                        y: pos.y.rem_euclid(self.height as i64),
                    },
                    dir.clone(),
                );
            }
        }

        fn next(&self, pos: &Vec2d, dir: &Direction) -> (Vec2d, char, Direction) {
            // Move along
            let mut cur = pos.clone();
            let mut cur_dir = dir.clone();
            loop {
                let d = match dir {
                    Direction::Right => &Vec2d::X,
                    Direction::Left => &Vec2d::XM,
                    Direction::Up => &Vec2d::YM,
                    Direction::Down => &Vec2d::Y,
                };
                // Add dir
                cur = cur.add(d);
                // Wrap to bounds
                (cur, cur_dir) = self.warp(&cur, &cur_dir);
                // Fetch to see if we're inside
                let c = self.get(&cur);
                if self.cube_mode {
                    // In cube mode, no iteration needed
                    if c == ' ' {
                        std::panic::panic_any("nyi");
                    }
                    return (cur, c, cur_dir);
                } else if c != ' ' {
                    return (cur, c, cur_dir);
                }
            }
        }

        fn follow(&self) -> (Vec2d, Direction) {
            let mut cur = self.start();
            for i in self.instructions.as_slice() {
                match i {
                    Instruction::Steps(steps) => {
                        for _ in 0..*steps {
                            let (next, c, dir) = self.next(&cur.0, &cur.1);
                            if c == '.' {
                                cur.0 = next;
                                cur.1 = dir;
                            } else {
                                break;
                            }
                        }
                    }
                    Instruction::RotateLeft => cur.1 = rotate_left(cur.1.clone()),
                    Instruction::RotateRight => cur.1 = rotate_right(cur.1.clone()),
                }
            }
            return cur;
        }

        fn verify_cube(&self) -> Res<()> {
            // Go through every cell, check that all movements are symmetric
            for y in 0..self.height as i64 {
                for x in 0..self.width as i64 {
                    let pos = Vec2d { x, y };
                    if self.get(&pos) == ' ' {
                        continue;
                    }
                    for dir in [
                        Direction::Up,
                        Direction::Left,
                        Direction::Down,
                        Direction::Right,
                    ] {
                        let fwd = self.next(&pos, &dir);
                        if fwd.1 == ' ' {
                            panic!("This isn't right 1!");
                        }
                        let back = self.next(&fwd.0, &rotate_left(rotate_left(fwd.2)));
                        if back.1 == ' ' {
                            panic!("This isn't right 2!");
                        }
                        if back.0.x != x || back.0.y != y {
                            panic!("This isn't right 3!");
                        }
                        if rotate_left(rotate_left(back.2)) != dir {
                            panic!("This isn't right 4!");
                        }
                    }
                }
            }
            return Ok(());
            // TODO(VS)
        }
    }

    fn score(pos: (Vec2d, Direction)) -> i64 {
        let dir = match pos.1 {
            Direction::Right => 0,
            Direction::Down => 1,
            Direction::Left => 2,
            Direction::Up => 3,
        };
        return (pos.0.y + 1) * 1000 + (pos.0.x + 1) * 4 + dir;
    }

    fn solve_a(x: &str) -> Res<i64> {
        let ctx = Ctx::parse(x)?;
        let pos = ctx.follow();
        return Ok(score(pos));
    }

    fn solve_b(x: &str) -> Res<i64> {
        let mut ctx = Ctx::parse(x)?;
        ctx.cube_mode = true;
        ctx.verify_cube()?;
        let pos = ctx.follow();
        return Ok(score(pos));
    }

    static TEST_CASE: &str = "        ...#\n        .#..\n        #...\n        ....\n...#.......#\n........#...\n..#....#....\n..........#.\n        ...#....\n        .....#..\n        .#......\n        ......#.\n\n10R5L5R10L4R5L5\n";

    #[test]
    fn test_a() -> Res<()> {
        let ctx = Ctx::parse(TEST_CASE)?;
        assert_eq!(
            ctx.next(&Vec2d { x: 0, y: 4 }, &Direction::Right),
            (Vec2d { x: 1, y: 4 }, '.', Direction::Right)
        );
        assert_eq!(
            ctx.next(&Vec2d { x: 0, y: 4 }, &Direction::Left),
            (Vec2d { x: 11, y: 4 }, '#', Direction::Left)
        );
        assert_eq!(
            ctx.next(&Vec2d { x: 0, y: 4 }, &Direction::Up),
            (Vec2d { x: 0, y: 7 }, '.', Direction::Up)
        );
        assert_eq!(
            ctx.next(&Vec2d { x: 0, y: 4 }, &Direction::Down),
            (Vec2d { x: 0, y: 5 }, '.', Direction::Down)
        );
        assert_eq!(solve_a(TEST_CASE)?, 6032);
        load_and_run_result(22, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        let mut ctx = Ctx::parse(TEST_CASE)?;
        ctx.cube_mode = true;
        assert_eq!(solve_b(TEST_CASE)?, 5031);
        load_and_run_result(22, 'b', solve_b);
        return Ok(());
    }
}
