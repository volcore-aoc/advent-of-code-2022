#[cfg(test)]
mod day202211 {
    use crate::file::{load_and_run_result, Res};

    #[derive(Debug)]
    struct Ctx {
        monkeys: Vec<Monkey>,
        divisor: i64,
        period: i64,
    }

    #[derive(Debug)]
    struct Monkey {
        inspect_count: usize,
        items: Vec<i64>,
        rules: MonkeyRules,
    }

    #[derive(Debug, Copy, Clone)]
    struct MonkeyRules {
        operation: Operation,
        test: i64,
        if_true: usize,
        if_false: usize,
    }

    #[derive(Debug, Copy, Clone)]
    enum Operation {
        Unknown,
        Square,
        Add(i64),
        Mul(i64),
    }

    impl Ctx {
        fn parse(x: &str) -> Res<Ctx> {
            let mut monkeys = vec![];
            let mut period = 1i64;
            for line in x.lines() {
                if line.trim().len() == 0 {
                    continue;
                }
                if line.starts_with("Monkey ") {
                    monkeys.push(Monkey {
                        rules: MonkeyRules {
                            if_false: 0,
                            if_true: 0,
                            operation: Operation::Unknown,
                            test: 0,
                        },
                        inspect_count: 0,
                        items: Vec::new(),
                    });
                    continue;
                }
                let monkey = match monkeys.last_mut() {
                    Some(x) => x,
                    _ => return Err("Expected 'Monkey'".into()),
                };
                if line.starts_with("  Starting items: ") {
                    let parts = line[18..].split(",");
                    for part in parts {
                        monkey.items.push(part.trim().parse::<i64>()?);
                    }
                    continue;
                }
                if line.starts_with("  Operation: new = old * old") {
                    monkey.rules.operation = Operation::Square;
                    continue;
                }
                if line.starts_with("  Operation: new = old * ") {
                    monkey.rules.operation = Operation::Mul(line[25..].trim().parse::<i64>()?);
                    continue;
                }
                if line.starts_with("  Operation: new = old + ") {
                    monkey.rules.operation = Operation::Add(line[25..].trim().parse::<i64>()?);
                    continue;
                }
                if line.starts_with("  Test: divisible by ") {
                    monkey.rules.test = line[21..].trim().parse::<i64>()?;
                    period *= monkey.rules.test;
                    continue;
                }
                if line.starts_with("    If true: throw to monkey") {
                    monkey.rules.if_true = line[28..].trim().parse::<usize>()?;
                    continue;
                }
                if line.starts_with("    If false: throw to monkey") {
                    monkey.rules.if_false = line[29..].trim().parse::<usize>()?;
                    continue;
                }
                return Err(line.into());
            }
            return Ok(Ctx {
                monkeys,
                divisor: 3,
                period,
            });
        }

        fn step_monkey(&mut self, i: usize) -> Res<()> {
            let monkeys = &mut self.monkeys;
            let rules = monkeys[i].rules.clone();
            let items = std::mem::take(&mut monkeys[i].items);
            for item in items.as_slice() {
                let worry_level = match rules.operation {
                    Operation::Square => item * item,
                    Operation::Add(x) => item + x,
                    Operation::Mul(x) => item * x,
                    _ => return Err("unknown operation".into()),
                } / self.divisor;
                let index = if worry_level % rules.test == 0 {
                    rules.if_true
                } else {
                    rules.if_false
                };
                monkeys[index].items.push(worry_level % self.period);
            }
            monkeys[i].inspect_count += items.len();
            return Ok(());
        }

        fn step(&mut self) -> Res<()> {
            for i in 0..self.monkeys.len() {
                self.step_monkey(i)?;
            }
            return Ok(());
        }

        fn result(&self) -> Res<usize> {
            let mut list: Vec<usize> = self
                .monkeys
                .as_slice()
                .into_iter()
                .map(|x| x.inspect_count)
                .collect();
            list.sort();
            return Ok(list[list.len() - 1] * list[list.len() - 2]);
        }

        fn run_a(&mut self) -> Res<usize> {
            for _ in 0..20 {
                self.step()?;
            }
            return self.result();
        }

        fn run_b(&mut self) -> Res<usize> {
            for _ in 0..10000 {
                self.step()?;
            }
            return self.result();
        }
    }

    fn solve_a(x: &str) -> Res<usize> {
        let mut ctx = Ctx::parse(x)?;
        return ctx.run_a();
    }

    fn solve_b(x: &str) -> Res<usize> {
        let mut ctx = Ctx::parse(x)?;
        ctx.divisor = 1;
        return ctx.run_b();
    }

    static TEST_CASE: &str = "Monkey 0:\n  Starting items: 79, 98\n  Operation: new = old * 19\n  Test: divisible by 23\n    If true: throw to monkey 2\n    If false: throw to monkey 3\n\nMonkey 1:\n  Starting items: 54, 65, 75, 74\n  Operation: new = old + 6\n  Test: divisible by 19\n    If true: throw to monkey 2\n    If false: throw to monkey 0\n\nMonkey 2:\n  Starting items: 79, 60, 97\n  Operation: new = old * old\n  Test: divisible by 13\n    If true: throw to monkey 1\n    If false: throw to monkey 3\n\nMonkey 3:\n  Starting items: 74\n  Operation: new = old + 3\n  Test: divisible by 17\n    If true: throw to monkey 0\n    If false: throw to monkey 1\n";

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(solve_a(TEST_CASE)?, 10605);
        load_and_run_result(11, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b(TEST_CASE)?, 2713310158);
        load_and_run_result(11, 'b', solve_b);
        return Ok(());
    }
}
