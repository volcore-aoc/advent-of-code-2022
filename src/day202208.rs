#[cfg(test)]
mod day202208 {
    use crate::file::{load_and_run_result, Res};

    #[derive(Debug)]
    struct Grid {
        grid: Vec<i8>,
        visibility: Vec<bool>,
        width: usize,
        height: usize,
    }

    impl Grid {
        fn parse(x: &str) -> Res<Grid> {
            let mut grid = vec![];
            let width = x.lines().nth(0).ok_or("invalid input")?.len();
            for l in x.lines() {
                if l.is_empty() {
                    continue;
                }
                for c in l.chars() {
                    grid.push(c as i8 - '0' as i8);
                }
            }
            let height = grid.len() / width;
            return Ok(Grid {
                grid,
                width,
                height,
                visibility: vec![false; width * height],
            });
        }

        fn vis(&mut self) {
            // Left to right
            for y in 0..self.height {
                let mut h = -1;
                for x in 0..self.width {
                    let v = self.grid[x + y * self.width];
                    if v > h {
                        self.visibility[x + y * self.width] = true;
                        h = v;
                    }
                }
            }
            // Right to left
            for y in 0..self.height {
                let mut h = -1;
                for x in (0..self.width).rev() {
                    let v = self.grid[x + y * self.width];
                    if v > h {
                        self.visibility[x + y * self.width] = true;
                        h = v;
                    }
                }
            }
            // top to bottom
            for x in 0..self.width {
                let mut h = -1;
                for y in 0..self.height {
                    let v = self.grid[x + y * self.width];
                    if v > h {
                        self.visibility[x + y * self.width] = true;
                        h = v;
                    }
                }
            }
            // bottom to top
            for x in 0..self.width {
                let mut h = -1;
                for y in (0..self.height).rev() {
                    let v = self.grid[x + y * self.width];
                    if v > h {
                        self.visibility[x + y * self.width] = true;
                        h = v;
                    }
                }
            }
        }

        fn count_a(&self) -> i64 {
            let mut count = 0i64;
            for v in self.visibility.as_slice() {
                if *v {
                    count += 1;
                }
            }
            return count;
        }

        fn val(&self, x: usize, y: usize) -> i8 {
            return self.grid[x + y * self.width];
        }

        fn trace_scenic(&self, cx: i64, cy: i64, dx: i64, dy: i64, max: i8) -> i64 {
            let mut count = 0i64;
            let mut cur = -1;
            let mut x = cx + dx;
            let mut y = cy + dy;
            loop {
                if x < 0 || y < 0 || x >= self.width as i64 || y >= self.height as i64 {
                    return count;
                }
                let next = self.val(x as usize, y as usize);
                count += 1;
                if next >= cur {
                    cur = next;
                }
                if next >= max {
                    return count;
                }
                x += dx;
                y += dy;
            }
        }

        fn scenic_score(&self, cx: i64, cy: i64) -> i64 {
            let max = self.val(cx as usize, cy as usize);
            let top = self.trace_scenic(cx, cy, 0, -1, max);
            let left = self.trace_scenic(cx, cy, -1, 0, max);
            let right = self.trace_scenic(cx, cy, 1, 0, max);
            let bottom = self.trace_scenic(cx, cy, 0, 1, max);
            let score = top * left * right * bottom;
            return score;
        }

        fn find_b(&self) -> i64 {
            let mut best = 0i64;
            for y in 1..self.height - 1 {
                for x in 1..self.width - 1 {
                    let score = self.scenic_score(x as i64, y as i64);
                    if score > best {
                        best = score;
                    }
                }
            }
            return best;
        }

        #[allow(dead_code)]
        fn dump(&self) {
            for y in 0..self.height {
                for x in 0..self.width {
                    print!("{}", self.grid[x + y * self.width]);
                }
                println!();
            }
            println!();
            for y in 0..self.height {
                for x in 0..self.width {
                    print!(
                        "{}",
                        if self.visibility[x + y * self.width] {
                            '1'
                        } else {
                            '0'
                        }
                    );
                }
                println!();
            }
        }
    }

    fn solve_a(x: &str) -> Res<i64> {
        let mut grid = Grid::parse(x)?;
        grid.vis();
        return Ok(grid.count_a());
    }

    fn solve_b(x: &str) -> Res<i64> {
        let grid = Grid::parse(x)?;
        return Ok(grid.find_b());
    }

    static TEST_CASE: &str = "30373\n25512\n65332\n33549\n35390\n";

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(solve_a(TEST_CASE)?, 21);
        load_and_run_result(8, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b(TEST_CASE)?, 8);
        load_and_run_result(8, 'b', solve_b);
        return Ok(());
    }
}
