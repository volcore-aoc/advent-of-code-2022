#[cfg(test)]
mod day202213 {
    use crate::file::{load_and_run_result, Res};
    use std::cmp::Ordering;

    #[derive(Debug)]
    struct Ctx {
        signals: Vec<Value>,
    }

    #[derive(Debug, Clone, PartialEq, Eq)]
    enum Value {
        Val(i64),
        Array(Vec<Value>),
    }

    #[derive(PartialEq)]
    enum Result {
        Continue,
        WrongOrder,
        RightOrder,
    }

    impl Ord for Value {
        fn cmp(&self, other: &Self) -> Ordering {
            match Ctx::right_order(self, other) {
                Result::Continue => Ordering::Equal,
                Result::RightOrder => Ordering::Less,
                Result::WrongOrder => Ordering::Greater,
            }
        }
    }

    impl PartialOrd for Value {
        fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
            Some(self.cmp(other))
        }
    }

    impl Ctx {
        fn parse_value(x: &mut std::iter::Peekable<std::str::Chars>) -> Res<Value> {
            match x.peek() {
                // This is an array
                Some('[') => {
                    x.next();
                    let mut vec = Vec::new();
                    while x.peek().is_some() {
                        match x.peek() {
                            Some(']') => {
                                x.next();
                                return Ok(Value::Array(vec));
                            }
                            Some(',') => {
                                x.next();
                                continue;
                            }
                            Some(_) => {
                                vec.push(Self::parse_value(x)?);
                                continue;
                            }
                            _ => return Err("EOL".into()),
                        }
                    }
                    return Ok(Value::Array(vec));
                }
                // This is a value
                Some(_) => {
                    // Read until we hit , or ]
                    let mut s = String::new();
                    while x.peek().is_some() {
                        match x.peek() {
                            Some(',') => break,
                            Some(']') => break,
                            Some(c) => {
                                s.push(*c);
                                x.next();
                            }
                            _ => return Err("EOL".into()),
                        };
                    }
                    return Ok(Value::Val(s.parse::<i64>()?));
                }
                // End of stream
                _ => return Err("EOL".into()),
            };
        }

        fn parse(x: &str) -> Res<Ctx> {
            let mut signals = Vec::new();
            for line in x.lines() {
                if line.is_empty() {
                    continue;
                }
                let signal = Self::parse_value(&mut line.chars().peekable())?;
                signals.push(signal);
            }
            return Ok(Ctx { signals });
        }

        fn ensure_array(v: &Value) -> Vec<Value> {
            return match v {
                Value::Val(x) => vec![Value::Val(*x)],
                Value::Array(x) => x.clone(),
            };
        }

        fn ensure_value(v: &Value) -> i64 {
            return match v {
                Value::Val(x) => *x,
                _ => 0,
            };
        }

        fn is_array(v: &Value) -> bool {
            return match v {
                Value::Array(_) => true,
                _ => false,
            };
        }

        fn right_order(left: &Value, right: &Value) -> Result {
            // Is either an array?
            if Ctx::is_array(left) || Ctx::is_array(right) {
                // Compare arrays
                let vl = Ctx::ensure_array(left);
                let vr = Ctx::ensure_array(right);
                let zipped = vl.iter().zip(vr.iter());
                for (l, r) in zipped {
                    match Ctx::right_order(l, r) {
                        Result::Continue => continue,
                        x => return x,
                    }
                }
                // All items were continue, make sure vl and vr are the same size
                if vl.len() == vr.len() {
                    return Result::Continue;
                } else if vl.len() < vr.len() {
                    return Result::RightOrder;
                } else {
                    return Result::WrongOrder;
                }
            } else {
                // Compare values
                let vl = Ctx::ensure_value(left);
                let vr = Ctx::ensure_value(right);
                if vl < vr {
                    return Result::RightOrder;
                } else if vl == vr {
                    return Result::Continue;
                } else {
                    return Result::WrongOrder;
                }
            }
        }
    }

    fn solve_a(x: &str) -> Res<usize> {
        let ctx = Ctx::parse(x)?;
        let mut count = 0;
        for i in 0..ctx.signals.len() / 2 {
            let res = Ctx::right_order(&ctx.signals[2 * i + 0], &ctx.signals[2 * i + 1]);
            if res != Result::WrongOrder {
                count += i + 1;
            }
        }
        return Ok(count);
    }

    fn solve_b(x: &str) -> Res<usize> {
        let ctx = Ctx::parse(x)?;
        let mut signals = ctx.signals.clone();
        let div2: Value = Value::Array(vec![Value::Array(vec![Value::Val(2)])]);
        let div6: Value = Value::Array(vec![Value::Array(vec![Value::Val(6)])]);
        signals.push(div2.clone());
        signals.push(div6.clone());
        signals.sort();
        // Compute score by finding the divs
        let mut score = 1;
        for (i, signal) in signals.iter().enumerate() {
            if signal.eq(&div2) || signal.eq(&div6) {
                score *= i + 1;
            }
        }
        return Ok(score);
    }

    static TEST_CASE: &str = "[1,1,3,1,1]\n[1,1,5,1,1]\n\n[[1],[2,3,4]]\n[[1],4]\n\n[9]\n[[8,7,6]]\n\n[[4,4],4,4]\n[[4,4],4,4,4]\n\n[7,7,7,7]\n[7,7,7]\n\n[]\n[3]\n\n[[[]]]\n[[]]\n\n[1,[2,[3,[4,[5,6,7]]]],8,9]\n[1,[2,[3,[4,[5,6,0]]]],8,9]\n";

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(solve_a(TEST_CASE)?, 13);
        load_and_run_result(13, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b(TEST_CASE)?, 140);
        load_and_run_result(13, 'b', solve_b);
        return Ok(());
    }
}
