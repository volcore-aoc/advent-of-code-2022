#[cfg(test)]
mod day202214 {
    use crate::file::{load_and_run_result, Res};
    use crate::vec2d::Vec2d;
    use std::collections::HashMap;

    #[derive(Debug)]
    struct Ctx {
        spans: Vec<Span>,
        map: HashMap<Vec2d, char>,
        low_y: i64,
    }

    #[derive(Debug)]
    struct Span {
        points: Vec<Vec2d>,
    }

    impl Ctx {
        fn parse(x: &str) -> Res<Ctx> {
            let mut spans = Vec::new();
            for line in x.lines() {
                let parts = line.split(" -> ");
                let mut points = Vec::new();
                for part in parts {
                    let p = part.parse::<Vec2d>()?;
                    points.push(p);
                }
                spans.push(Span { points });
            }
            return Ok(Ctx {
                spans,
                map: HashMap::new(),
                low_y: i64::MAX,
            });
        }

        fn draw_spans(&mut self) {
            for span in self.spans.as_slice() {
                let mut curr = span.points[0];
                self.map.insert(curr, '#');
                for p in &span.points[1..] {
                    // Compute delta
                    let dir = p.sub(&curr).signum();
                    // Note: this requires lines to be either straight or diagonal. Other lines no
                    // worky
                    while !curr.eq(p) {
                        curr = curr.add(&dir);
                        self.map.insert(curr, '#');
                    }
                }
            }
            // Store the highest y value as the boundary beyond which a sand is "dropped"
            self.low_y = self.minmax().1.y + 2;
        }

        fn minmax(&self) -> (Vec2d, Vec2d) {
            let mut min: Vec2d = Vec2d::MAX;
            let mut max: Vec2d = Vec2d::MIN;
            for (p, _) in self.map.iter() {
                min = min.min(p);
                max = max.max(p);
            }
            return (min, max);
        }

        #[allow(dead_code)]
        fn draw(&self) {
            // find min and max
            let (min, max) = self.minmax();
            for y in min.y - 2..=max.y + 2 {
                for x in min.x - 2..=max.x + 2 {
                    print!("{}", self.get(Vec2d { x, y }));
                }
                println!();
            }
        }

        fn get(&self, s: Vec2d) -> &char {
            if s.y >= self.low_y {
                return &'%';
            }
            return self.map.get(&s).unwrap_or(&'.');
        }

        fn sim(&mut self, s: Vec2d) -> bool {
            if s.y >= self.low_y - 1 {
                self.map.insert(s, 'o');
                return false;
            }
            // check directly below
            if *self.get(s.offset(0, 1)) == '.' {
                return self.sim(s.offset(0, 1));
            }
            // check lower left
            if *self.get(s.offset(-1, 1)) == '.' {
                return self.sim(s.offset(-1, 1));
            }
            // check lower right
            if *self.get(s.offset(1, 1)) == '.' {
                return self.sim(s.offset(1, 1));
            }
            // No more moving, draw
            self.map.insert(s, 'o');
            return true;
        }

        fn count(&self) -> usize {
            let mut count = 0;
            for (_, c) in self.map.iter() {
                if *c == 'o' {
                    count += 1;
                }
            }
            return count;
        }
    }

    fn solve_a(x: &str) -> Res<usize> {
        let mut ctx = Ctx::parse(x)?;
        ctx.draw_spans();
        // Run until convergence
        loop {
            if ctx.sim(Vec2d { x: 500, y: 0 }) == false {
                break;
            }
        }
        // ctx.draw();
        return Ok(ctx.count() - 1);
    }

    fn solve_b(x: &str) -> Res<usize> {
        let mut ctx = Ctx::parse(x)?;
        let source = Vec2d { x: 500, y: 0 };
        ctx.draw_spans();
        loop {
            ctx.sim(source);
            if *ctx.get(source) == 'o' {
                break;
            }
        }
        // ctx.draw();
        return Ok(ctx.count());
    }

    static TEST_CASE: &str = "498,4 -> 498,6 -> 496,6\n503,4 -> 502,4 -> 502,9 -> 494,9\n";

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(solve_a(TEST_CASE)?, 24);
        load_and_run_result(14, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b(TEST_CASE)?, 93);
        load_and_run_result(14, 'b', solve_b);
        return Ok(());
    }
}
