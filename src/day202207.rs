#[cfg(test)]
mod day202207 {
    use crate::file::{load_and_run_result, Res};

    #[derive(Debug)]
    struct Entry {
        name: String,
        size: i64,
        children: Vec<Entry>,
    }

    impl Entry {
        fn insert(&mut self, path: &[String], entry: Entry) {
            if path.is_empty() {
                // Add to children
                self.children.push(entry)
            } else {
                // Recurse
                for c in &mut self.children {
                    if c.name == path[0] {
                        c.insert(&path[1..], entry);
                        return;
                    }
                }
            }
        }

        #[allow(dead_code)]
        fn dump(&self, depth: usize) {
            if !self.children.is_empty() {
                println!(
                    "{:indent$}- {} (dir, size={})",
                    "",
                    self.name,
                    self.size,
                    indent = depth * 2
                );
            } else {
                println!(
                    "{:indent$}- {} (file, size={})",
                    "",
                    self.name,
                    self.size,
                    indent = depth * 2
                );
            }
            for c in self.children.as_slice() {
                c.dump(depth + 1);
            }
        }

        fn compute_size(&mut self) -> i64 {
            if self.children.is_empty() {
                return self.size;
            }
            let mut size = 0i64;
            for c in &mut self.children {
                size += c.compute_size();
            }
            self.size = size;
            return size;
        }

        fn count_a(&self) -> i64 {
            if self.children.is_empty() {
                return 0i64;
            }
            let mut total = 0i64;
            for c in self.children.as_slice() {
                total += c.count_a();
            }
            if self.size < 100000 {
                total += self.size;
            }
            return total;
        }

        fn find_b(&self, needed: i64) -> i64 {
            if self.children.is_empty() {
                return i64::MAX;
            }
            let mut lowest = i64::MAX;
            for c in self.children.as_slice() {
                let v = c.find_b(needed);
                if v < lowest {
                    lowest = v;
                }
            }
            if self.size > needed && self.size < lowest {
                lowest = self.size;
            }
            return lowest;
        }
    }

    fn parse(x: &str) -> Res<Entry> {
        let mut root: Entry = Entry {
            name: "/".into(),
            children: Vec::new(),
            size: 0,
        };
        let mut path: Vec<String> = vec![];

        for line in x.lines() {
            if line.starts_with("$ ") {
                if line == "$ cd /" || line == "$ ls" {
                    continue;
                }
                if line.starts_with("$ cd ") {
                    let dir = &line[5..];
                    if dir == ".." {
                        path.pop();
                    } else {
                        path.push(dir.into());
                    }
                }
                continue;
            }
            if line.starts_with("dir ") {
                let dir = &line[4..];
                root.insert(
                    &path,
                    Entry {
                        children: Vec::new(),
                        size: 0,
                        name: dir.into(),
                    },
                );
                continue;
            }
            // File command
            let parts: Vec<&str> = line.split(" ").collect();
            if parts.len() != 2 {
                return Err("file with len != 2".into());
            }
            let size = parts[0].parse::<i64>()?;
            let name = parts[1];
            root.insert(
                &path,
                Entry {
                    children: Vec::new(),
                    size,
                    name: name.into(),
                },
            );
        }
        return Ok(root);
    }

    fn solve_a(x: &str) -> Res<i64> {
        let mut root = parse(x)?;
        root.compute_size();
        return Ok(root.count_a());
    }

    fn solve_b(x: &str) -> Res<i64> {
        let mut root = parse(x)?;
        root.compute_size();
        let total = 70000000;
        let required = 30000000;
        let used = root.size;
        let needed = required - (total - used);
        return Ok(root.find_b(needed));
    }

    static TEST_CASE: &str = "$ cd /\n$ ls\ndir a\n14848514 b.txt\n8504156 c.dat\ndir d\n$ cd a\n$ ls\ndir e\n29116 f\n2557 g\n62596 h.lst\n$ cd e\n$ ls\n584 i\n$ cd ..\n$ cd ..\n$ cd d\n$ ls\n4060174 j\n8033020 d.log\n5626152 d.ext\n7214296 k\n";

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(solve_a(TEST_CASE)?, 95437);
        load_and_run_result(7, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b(TEST_CASE)?, 24933642);
        load_and_run_result(7, 'b', solve_b);
        return Ok(());
    }
}
