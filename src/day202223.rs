#[cfg(test)]
mod day202223 {
    use crate::file::{load_and_run_result, Res};
    use crate::vec2d::Vec2d;
    use std::collections::{HashMap, HashSet};

    #[derive(Debug)]
    struct Ctx {
        elves: HashSet<Vec2d>,
        round: usize,
    }

    impl Ctx {
        fn parse(input: &str) -> Res<Ctx> {
            let mut elves = HashSet::new();
            for (y, line) in input.lines().enumerate() {
                for (x, c) in line.chars().enumerate() {
                    if c == '#' {
                        elves.insert(Vec2d {
                            x: x as i64,
                            y: y as i64,
                        });
                    }
                }
            }
            return Ok(Ctx { elves, round: 0 });
        }

        fn is_free(&self, pos: &Vec2d, dx: i64, dy: i64) -> bool {
            return !self.elves.contains(&pos.add(&Vec2d { x: dx, y: dy }));
        }

        fn next_move(&self, pos: &Vec2d) -> Vec2d {
            let nw = self.is_free(pos, -1, -1);
            let nn = self.is_free(pos, 0, -1);
            let ne = self.is_free(pos, 1, -1);
            let ww = self.is_free(pos, -1, 0);
            let ee = self.is_free(pos, 1, 0);
            let sw = self.is_free(pos, -1, 1);
            let ss = self.is_free(pos, 0, 1);
            let se = self.is_free(pos, 1, 1);
            if nw && nn && ne && ww && ee && sw && ss && se {
                // Don't move
                return pos.clone();
            }
            for i in 0..=3 {
                match (i + self.round) % 4 {
                    0 => {
                        if nw && nn && ne {
                            return pos.add(&Vec2d { x: 0, y: -1 });
                        }
                    }
                    1 => {
                        if sw && ss && se {
                            return pos.add(&Vec2d { x: 0, y: 1 });
                        }
                    }
                    2 => {
                        if ww && nw && sw {
                            return pos.add(&Vec2d { x: -1, y: 0 });
                        }
                    }
                    3 => {
                        if ee && ne && se {
                            return pos.add(&Vec2d { x: 1, y: 0 });
                        }
                    }
                    _ => {}
                }
            }
            // Don't move
            return pos.clone();
        }

        fn step(&mut self) -> bool {
            let mut counter: HashMap<Vec2d, usize> = HashMap::new();
            let mut did_move = false;
            // Create counter map
            for pos in self.elves.iter() {
                let new_pos = self.next_move(pos);
                counter.insert(new_pos, counter.get(&new_pos).unwrap_or(&0) + 1);
            }
            // Now move
            let mut new_elves = HashSet::new();
            for pos in self.elves.iter() {
                let new_pos = self.next_move(pos);
                let actual_pos = match counter.get(&new_pos) {
                    Some(1) => new_pos,
                    _ => pos.clone(),
                };
                if !new_pos.eq(pos) {
                    did_move = true;
                }
                new_elves.insert(actual_pos);
            }
            // Finish
            self.elves = new_elves;
            self.round += 1;
            return did_move;
        }

        fn run(&mut self, rounds: usize) {
            for _ in 0..rounds {
                self.step();
                // self.dump();
            }
        }

        fn run_b(&mut self) {
            loop {
                if !self.step() {
                    return;
                }
            }
        }

        #[allow(dead_code)]
        fn dump(&self) {
            // Get min & max
            let mut min: Vec2d = Vec2d::MAX;
            let mut max: Vec2d = Vec2d::MIN;
            for pos in self.elves.iter() {
                min = min.min(pos);
                max = max.max(pos);
            }
            for y in min.y..=max.y {
                for x in min.x..=max.x {
                    if self.is_free(&Vec2d { x, y }, 0, 0) {
                        print!(".");
                    } else {
                        print!("#");
                    }
                }
                println!();
            }
        }

        fn count_a(&self) -> usize {
            // Get min & max
            let mut min: Vec2d = Vec2d::MAX;
            let mut max: Vec2d = Vec2d::MIN;
            for pos in self.elves.iter() {
                min = min.min(pos);
                max = max.max(pos);
            }
            // Measure the entire area and subtract all elves
            let wh = max.sub(&min).add(&Vec2d::ONE);
            return (wh.x * wh.y) as usize - self.elves.len();
        }
    }

    fn solve_a(x: &str) -> Res<usize> {
        let mut ctx = Ctx::parse(x)?;
        ctx.run(10);
        return Ok(ctx.count_a());
    }

    fn solve_b(x: &str) -> Res<usize> {
        let mut ctx = Ctx::parse(x)?;
        ctx.run_b();
        return Ok(ctx.round);
    }

    static TEST_CASE: &str = "....#..\n..###.#\n#...#.#\n.#...##\n#.###..\n##.#.##\n.#..#..\n";

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(solve_a(TEST_CASE)?, 110);
        load_and_run_result(23, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b(TEST_CASE)?, 20);
        load_and_run_result(23, 'b', solve_b);
        return Ok(());
    }
}
