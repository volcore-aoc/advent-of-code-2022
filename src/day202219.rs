#[cfg(test)]
mod day202219 {
    use crate::file::{load_and_run_result, Res};
    use std::cmp::{max, min};

    #[derive(Debug)]
    struct Ctx {
        blueprints: Vec<Blueprint>,
    }

    #[derive(Debug, Copy, Clone, Default)]
    struct Resources {
        ore: usize,
        clay: usize,
        obs: usize,
        geo: usize,
    }

    #[derive(Debug)]
    struct Blueprint {
        ore_cost: Resources,
        clay_cost: Resources,
        obs_cost: Resources,
        geo_cost: Resources,
        max_cost: Resources,
    }

    #[derive(Debug, Copy, Clone)]
    struct State {
        res: Resources,
        bots: Resources,
    }

    impl Resources {
        fn ore(amount: usize) -> Resources {
            return Resources {
                ore: amount,
                ..Default::default()
            };
        }

        fn clay(amount: usize) -> Resources {
            return Resources {
                clay: amount,
                ..Default::default()
            };
        }

        fn obs(amount: usize) -> Resources {
            return Resources {
                obs: amount,
                ..Default::default()
            };
        }

        fn geo(amount: usize) -> Resources {
            return Resources {
                geo: amount,
                ..Default::default()
            };
        }

        fn add(&self, other: &Resources) -> Resources {
            return Resources {
                geo: self.geo + other.geo,
                ore: self.ore + other.ore,
                obs: self.obs + other.obs,
                clay: self.clay + other.clay,
            };
        }

        fn sub(&self, other: &Resources) -> Resources {
            return Resources {
                geo: self.geo - other.geo,
                ore: self.ore - other.ore,
                obs: self.obs - other.obs,
                clay: self.clay - other.clay,
            };
        }

        fn ge(&self, other: &Resources) -> bool {
            return self.geo >= other.geo
                && self.ore >= other.ore
                && self.obs >= other.obs
                && self.clay >= other.clay;
        }

        fn max(&self, other: &Resources) -> Resources {
            return Resources {
                ore: max(self.ore, other.ore),
                clay: max(self.clay, other.clay),
                obs: max(self.obs, other.obs),
                geo: max(self.geo, other.geo),
            };
        }
    }

    impl State {
        fn integrate(&self) -> State {
            return State {
                res: self.res.add(&self.bots),
                bots: self.bots.clone(),
            };
        }
    }

    impl Blueprint {
        fn ql(&self, time: usize, state: State, max_geo: &mut usize) -> usize {
            if time == 0 {
                if state.res.geo > *max_geo {
                    *max_geo = state.res.geo;
                }
                return state.res.geo;
            }
            // What is the estimated max geo? If we just build geodes from here on out, we have
            // sum_(time+1) i many geodes, so t * (t+1)/2. But since production only starts next
            // update, we can reduce it by 1: (t-1) * t/2
            let max_est_geo = state.res.geo + state.bots.geo * time + time * (time - 1) / 2;
            if max_est_geo <= *max_geo {
                // Just return max_geo if we can't beat it
                return *max_geo;
            }
            let new_state = state.integrate();
            // Check if we can build any of the bots
            let mut best_geo = 0;
            let bots = [
                (self.geo_cost, Resources::geo(9999999), Resources::geo(1)),
                (
                    self.obs_cost,
                    Resources::obs(self.max_cost.obs),
                    Resources::obs(1),
                ),
                (
                    self.clay_cost,
                    Resources::clay(self.max_cost.clay),
                    Resources::clay(1),
                ),
                (
                    self.ore_cost,
                    Resources::ore(self.max_cost.ore),
                    Resources::ore(1),
                ),
            ];
            for (cost, max_needed, gain) in bots {
                // Can we build one and do we still potentially need more?
                if state.res.ge(&cost) && !state.bots.ge(&max_needed) {
                    let s = State {
                        res: new_state.res.sub(&cost),
                        bots: state.bots.add(&gain),
                    };
                    best_geo = max(self.ql(time - 1, s, max_geo), best_geo);
                }
            }
            // Base case: do nothing
            best_geo = max(self.ql(time - 1, new_state, max_geo), best_geo);
            return best_geo;
        }

        fn quality_level(&self, len: usize) -> usize {
            let mut max_geo: usize = 0;
            self.ql(
                len,
                State {
                    res: Default::default(),
                    bots: Resources::ore(1),
                },
                &mut max_geo,
            );
            return max_geo;
        }
    }

    impl Ctx {
        fn parse(x: &str) -> Res<Ctx> {
            let mut blueprints = Vec::new();
            for line in x.lines() {
                if line.len() == 0 {
                    continue;
                }
                let (_, right) = line.split_once(": ").ok_or("pe".to_string())?;
                let mut line2 = right.replace("Each ore robot costs ", "");
                line2 = line2.replace(" ore. Each clay robot costs ", ";");
                line2 = line2.replace(" ore. Each obsidian robot costs ", ";");
                line2 = line2.replace(" ore and ", ";");
                line2 = line2.replace(" clay. Each geode robot costs ", ";");
                line2 = line2.replace(" obsidian.", "");
                let parts: Result<Vec<usize>, _> =
                    line2.split(";").map(|x| x.parse::<usize>()).collect();
                let costs = parts?;
                let ore_cost = Resources::ore(costs[0]);
                let clay_cost = Resources::ore(costs[1]);
                let obs_cost = Resources::ore(costs[2]).add(&Resources::clay(costs[3]));
                let geo_cost = Resources::ore(costs[4]).add(&Resources::obs(costs[5]));
                let max_cost = ore_cost.max(&clay_cost.max(&obs_cost.max(&geo_cost)));
                blueprints.push(Blueprint {
                    ore_cost,
                    clay_cost,
                    obs_cost,
                    geo_cost,
                    max_cost,
                })
            }
            return Ok(Ctx { blueprints });
        }
    }

    fn solve_a(x: &str) -> Res<usize> {
        let ctx = Ctx::parse(x)?;
        let mut sum = 0;
        for (idx, bp) in ctx.blueprints.iter().enumerate() {
            let ql = bp.quality_level(24);
            sum += (idx + 1) * ql;
        }
        return Ok(sum);
    }

    fn solve_b(x: &str) -> Res<usize> {
        let ctx = Ctx::parse(x)?;
        let mut prod = 1;
        let bps = &ctx.blueprints[..min(3, ctx.blueprints.len())];
        for bp in bps.iter() {
            let ql = bp.quality_level(32);
            prod *= ql;
        }
        return Ok(prod);
    }

    static TEST_CASE: &str = "Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.\nBlueprint 2: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. Each geode robot costs 3 ore and 12 obsidian.\n";
    static TEST_CASE1: &str = "Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.\n";
    static TEST_CASE2: &str = "Blueprint 1: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. Each geode robot costs 3 ore and 12 obsidian.\n";

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(solve_a(TEST_CASE1)?, 9);
        assert_eq!(solve_a(TEST_CASE2)?, 12);
        assert_eq!(solve_a(TEST_CASE)?, 9 + 12 * 2);
        load_and_run_result(19, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b(TEST_CASE1)?, 56);
        assert_eq!(solve_b(TEST_CASE2)?, 62);
        assert_eq!(solve_b(TEST_CASE)?, 56 * 62);
        load_and_run_result(19, 'b', solve_b);
        return Ok(());
    }
}
