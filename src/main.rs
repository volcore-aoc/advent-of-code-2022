extern crate core;

mod day202201;
mod day202202;
mod day202203;
mod day202204;
mod day202205;
mod day202206;
mod day202207;
mod day202208;
mod day202209;
mod day202210;
mod day202211;
mod day202212;
mod day202213;
mod day202214;
mod day202215;
mod day202216;
mod day202217;
mod day202218;
mod day202219;
mod day202220;
mod day202221;
mod day202222;
mod day202223;
mod day202224;
mod day202225;
mod file;
mod vec2d;
mod vec3d;

fn main() {}
