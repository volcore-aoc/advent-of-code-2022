#[cfg(test)]
mod day202218 {
    use crate::file::{load_and_run_result, Res};
    use std::collections::{HashSet, VecDeque};

    #[derive(Debug)]
    struct Ctx {
        grid: HashSet<usize>,
    }

    fn idx(x: usize, y: usize, z: usize) -> usize {
        return x + y * 100 + z * 10000;
    }

    fn xyz(idx: usize) -> (usize, usize, usize) {
        let x = idx % 100;
        let y = (idx / 100) % 100;
        let z = idx / 10000;
        return (x, y, z);
    }

    impl Ctx {
        fn parse(x: &str) -> Res<Ctx> {
            let mut grid = HashSet::new();
            for line in x.lines() {
                if line.len() == 0 {
                    continue;
                }
                let parts: Vec<&str> = line.split(",").collect();
                // Shift by one to avoid negative indices
                let x = parts[0].parse::<usize>()? + 1;
                let y = parts[1].parse::<usize>()? + 1;
                let z = parts[2].parse::<usize>()? + 1;
                grid.insert(idx(x, y, z));
            }
            return Ok(Ctx { grid });
        }

        fn is_free(&self, x: usize, y: usize, z: usize) -> usize {
            let i = idx(x, y, z);
            return if self.grid.contains(&i) { 0 } else { 1 };
        }

        fn measure_surface(&self) -> usize {
            let mut count = 0;
            for &idx in self.grid.iter() {
                let (x, y, z) = xyz(idx);
                count += self.is_free(x - 1, y, z);
                count += self.is_free(x + 1, y, z);
                count += self.is_free(x, y - 1, z);
                count += self.is_free(x, y + 1, z);
                count += self.is_free(x, y, z - 1);
                count += self.is_free(x, y, z + 1);
            }
            return count;
        }

        fn fill_interior(&mut self) {
            let mut filled: HashSet<usize> = HashSet::new();
            let mut queue: VecDeque<usize> = VecDeque::from([0]);
            while let Some(i) = queue.pop_front() {
                if filled.contains(&i) {
                    continue;
                }
                // Check if it is free
                let (x, y, z) = xyz(i);
                if self.is_free(x, y, z) == 0 {
                    continue;
                }
                filled.insert(i);
                // Queue neighbours
                if x > 0 {
                    queue.push_back(idx(x - 1, y, z));
                }
                if x < 20 {
                    queue.push_back(idx(x + 1, y, z));
                }
                if y > 0 {
                    queue.push_back(idx(x, y - 1, z));
                }
                if y < 20 {
                    queue.push_back(idx(x, y + 1, z));
                }
                if z > 0 {
                    queue.push_back(idx(x, y, z - 1));
                }
                if z < 20 {
                    queue.push_back(idx(x, y, z + 1));
                }
            }
            // Now update the grid, inserting every point that's not filled
            for z in 1..19 {
                for y in 1..19 {
                    for x in 1..19 {
                        let i = idx(x, y, z);
                        if filled.contains(&i) {
                            continue;
                        }
                        self.grid.insert(i);
                    }
                }
            }
        }
    }

    fn solve_a(x: &str) -> Res<usize> {
        let ctx = Ctx::parse(x)?;
        return Ok(ctx.measure_surface());
    }

    fn solve_b(x: &str) -> Res<usize> {
        let mut ctx = Ctx::parse(x)?;
        ctx.fill_interior();
        return Ok(ctx.measure_surface());
    }

    static TEST_CASE: &str = "1,1,1\n2,1,1\n";
    static TEST_CASE2: &str = "2,2,2\n1,2,2\n3,2,2\n2,1,2\n2,3,2\n2,2,1\n2,2,3\n2,2,4\n2,2,6\n1,2,5\n3,2,5\n2,1,5\n2,3,5\n";

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(solve_a(TEST_CASE)?, 10);
        assert_eq!(solve_a(TEST_CASE2)?, 64);
        load_and_run_result(18, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b(TEST_CASE)?, 10);
        assert_eq!(solve_b(TEST_CASE2)?, 58);
        load_and_run_result(18, 'b', solve_b);
        return Ok(());
    }
}
