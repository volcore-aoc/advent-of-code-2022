#[cfg(test)]
mod day202212 {
    use crate::file::{load_and_run_result, Res};
    use crate::vec2d::Vec2d;
    use std::cmp::Ordering;
    use std::collections::BinaryHeap;

    #[derive(Debug)]
    struct Ctx {
        grid: Vec<i8>,
        dgrid: Vec<i64>,
        width: usize,
        height: usize,
        start: Vec2d,
        end: Vec2d,
    }

    #[derive(Copy, Clone, Eq, PartialEq)]
    struct HeapState {
        cost: i64,
        index: usize,
    }

    impl Ord for HeapState {
        fn cmp(&self, other: &Self) -> Ordering {
            // Flip around self.cmp(other) as we're interested in the **smallest**
            other
                .cost
                .cmp(&self.cost)
                .then_with(|| self.index.cmp(&other.index))
        }
    }

    impl PartialOrd for HeapState {
        fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
            Some(self.cmp(other))
        }
    }

    impl Ctx {
        fn parse(x: &str) -> Res<Ctx> {
            let mut grid = Vec::new();
            let mut width = 0;
            let mut height = 0;
            let mut start = Vec2d::ZERO;
            let mut end = Vec2d::ZERO;
            for (y, line) in x.lines().enumerate() {
                if line.len() <= 0 {
                    continue;
                }
                width = line.len();
                height = y + 1;
                for (x, c) in line.chars().enumerate() {
                    let h: i8 = if c == 'S' {
                        start = Vec2d {
                            x: x as i64,
                            y: y as i64,
                        };
                        0
                    } else if c == 'E' {
                        end = Vec2d {
                            x: x as i64,
                            y: y as i64,
                        };
                        'z' as i8 - 'a' as i8
                    } else {
                        c as i8 - 'a' as i8
                    };
                    grid.push(h);
                }
            }
            return Ok(Ctx {
                grid,
                width,
                height,
                start,
                end,
                dgrid: vec![-1; width * height],
            });
        }

        fn vec2idx(&self, p: Vec2d) -> Option<usize> {
            if p.x < 0 || p.y < 0 || p.x >= self.width as i64 || p.y >= self.height as i64 {
                return None;
            }
            return Some((p.x + p.y * self.width as i64) as usize);
        }

        fn idx2vec(&self, idx: usize) -> Vec2d {
            return Vec2d {
                x: (idx % self.width) as i64,
                y: (idx / self.width) as i64,
            };
        }

        fn v(&self, p: Vec2d) -> i8 {
            return match self.vec2idx(p) {
                Some(x) => self.grid[x],
                None => -1,
            };
        }

        fn d(&self, p: Vec2d) -> i64 {
            return match self.vec2idx(p) {
                Some(x) => self.dgrid[x],
                None => -1,
            };
        }

        #[allow(dead_code)]
        fn draw(&self) {
            for y in 0..self.height {
                for x in 0..self.height {
                    let p = Vec2d {
                        x: x as i64,
                        y: y as i64,
                    };
                    let val = (self.v(p) + 'a' as i8) as u8 as char;
                    print!("{}", val);
                }
                println!();
            }
            for y in 0..self.height {
                for x in 0..self.height {
                    let p = Vec2d {
                        x: x as i64,
                        y: y as i64,
                    };
                    let val = self.d(p);
                    print!("{:5} ", val);
                }
                println!();
            }
        }

        fn build_grid(&mut self, start: Vec2d) -> Res<()> {
            let neighbours = vec![
                Vec2d { x: -1, y: 0 },
                Vec2d { x: 1, y: 0 },
                Vec2d { x: 0, y: -1 },
                Vec2d { x: 0, y: 1 },
            ];
            let start_idx = self.vec2idx(start).unwrap_or(0);
            self.dgrid[start_idx] = 0;
            // Add all neighbors to the stack
            let mut heap = BinaryHeap::new();
            heap.push(HeapState {
                index: start_idx,
                cost: 0,
            });
            while let Some(HeapState { index, cost }) = heap.pop() {
                // Have we already inspected this in a closer way
                if cost > self.dgrid[index] {
                    continue;
                }
                // Inspect all neighbours
                let p = self.idx2vec(index);
                let v = self.grid[index];
                for n in neighbours.as_slice() {
                    let p2 = p.add(&n);
                    let idx2 = match self.vec2idx(p2) {
                        Some(x) => x,
                        None => continue,
                    };
                    let v2 = self.grid[idx2];
                    // Too high?
                    if v2 > v + 1 {
                        continue;
                    }
                    // Already cheaper?
                    let old_cost = self.dgrid[idx2];
                    let cost2 = cost + 1;
                    if old_cost != -1 && old_cost <= cost2 {
                        continue;
                    }
                    // Not cheaper
                    self.dgrid[idx2] = cost2;
                    // Push
                    heap.push(HeapState {
                        index: idx2,
                        cost: cost2,
                    });
                }
            }

            return Ok(());
        }
    }

    fn solve_a(x: &str) -> Res<i64> {
        let mut ctx = Ctx::parse(x)?;
        ctx.build_grid(ctx.start)?;
        return match ctx.d(ctx.end) {
            -1 => Err("no path to target".into()),
            x => Ok(x),
        };
    }

    fn solve_b(x: &str) -> Res<i64> {
        let mut ctx = Ctx::parse(x)?;
        let mut min = i64::MAX;
        for y in 0..ctx.height {
            for x in 0..ctx.width {
                let start = Vec2d {
                    x: x as i64,
                    y: y as i64,
                };
                if ctx.v(start) != 0 {
                    continue;
                }
                ctx.build_grid(start)?;
                let d = ctx.d(ctx.end);
                if d > 0 && d < min {
                    min = d;
                }
            }
        }
        return Ok(min);
    }

    static TEST_CASE: &str = "Sabqponm\nabcryxxl\naccszExk\nacctuvwj\nabdefghi\n";

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(solve_a(TEST_CASE)?, 31);
        load_and_run_result(12, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b(TEST_CASE)?, 29);
        load_and_run_result(12, 'b', solve_b);
        return Ok(());
    }
}
