#[cfg(test)]
mod day202220 {
    use crate::file::{load_and_run_result, Res};

    #[derive(Debug)]
    struct Ctx {
        values: Vec<i64>,
    }

    impl Ctx {
        fn parse(x: &str) -> Res<Ctx> {
            let mut values = Vec::new();
            for line in x.lines() {
                if line.len() == 0 {
                    continue;
                }
                values.push(line.parse::<i64>()?);
            }
            return Ok(Ctx { values });
        }

        fn mov(&self, perm: &mut Vec<usize>, which: usize, key: i64) -> Res<()> {
            let len = perm.len();
            // Find the position of the element
            let pos = perm
                .iter()
                .position(|&x| x == which)
                .ok_or("item not found".to_string())?;
            // Fetch the value
            let value = self.values[which] * key;
            let new_pos = (pos as i64 + value).rem_euclid(len as i64 - 1) as usize;
            // Remove the element from the array at the
            perm.remove(pos);
            // Insert at the new position
            perm.insert(new_pos, which);
            return Ok(());
        }

        fn solve(&self, key: i64, count: usize) -> Res<i64> {
            // Create permutation table
            let len = self.values.len();
            let mut perm: Vec<usize> = (0..len).into_iter().collect();
            // Now execute each instruction
            for _ in 0..count {
                for i in 0..len {
                    // Move this value forward or backward based on it's permutation info
                    self.mov(&mut perm, i, key)?;
                }
            }
            // Final sequence
            let vs: Vec<i64> = perm.iter().map(|&x| self.values[x] * key).collect();
            // Now find the 0 element as the start
            let pos0 = vs
                .iter()
                .position(|x| *x == 0)
                .ok_or("0 not found".to_string())?;
            let a = vs[(pos0 + 1000).rem_euclid(len)];
            let b = vs[(pos0 + 2000).rem_euclid(len)];
            let c = vs[(pos0 + 3000).rem_euclid(len)];
            return Ok(a + b + c);
        }
    }

    fn solve_a(x: &str) -> Res<i64> {
        let ctx = Ctx::parse(x)?;
        return ctx.solve(1, 1);
    }

    fn solve_b(x: &str) -> Res<i64> {
        let ctx = Ctx::parse(x)?;
        return ctx.solve(811589153, 10);
    }

    static TEST_CASE: &str = "1\n2\n-3\n3\n-2\n0\n4\n";

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(solve_a(TEST_CASE)?, 3);
        load_and_run_result(20, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b(TEST_CASE)?, 1623178306);
        load_and_run_result(20, 'b', solve_b);
        return Ok(());
    }
}
