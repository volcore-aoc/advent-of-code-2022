#[cfg(test)]
mod day202206 {
    use crate::file::{load_and_run_result, Res};
    use std::collections::HashSet;

    fn windowed(x: &str, l: usize) -> Res<i64> {
        for i in l..x.len() + 1 {
            let mut set: HashSet<char> = HashSet::new();
            let end = i;
            let start = i - l;
            for j in start..end {
                set.insert(x.chars().nth(j).ok_or("out of bounds")?);
            }
            if set.len() == l {
                return Ok(i as i64);
            }
        }
        return Err("no signal found".into());
    }

    fn solve_a(x: &str) -> Res<i64> {
        return windowed(x, 4);
    }

    fn solve_b(x: &str) -> Res<i64> {
        return windowed(x, 14);
    }

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(solve_a("mjqjpqmgbljsphdztnvjfqwrcgsmlb")?, 7);
        assert_eq!(solve_a("bvwbjplbgvbhsrlpgdmjqwftvncz")?, 5);
        assert_eq!(solve_a("nppdvjthqldpwncqszvftbrmjlhg")?, 6);
        assert_eq!(solve_a("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")?, 10);
        assert_eq!(solve_a("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")?, 11);
        load_and_run_result(6, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b("mjqjpqmgbljsphdztnvjfqwrcgsmlb")?, 19);
        assert_eq!(solve_b("bvwbjplbgvbhsrlpgdmjqwftvncz")?, 23);
        assert_eq!(solve_b("nppdvjthqldpwncqszvftbrmjlhg")?, 23);
        assert_eq!(solve_b("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")?, 29);
        assert_eq!(solve_b("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")?, 26);
        load_and_run_result(6, 'b', solve_b);
        return Ok(());
    }
}
