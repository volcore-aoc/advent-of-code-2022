#[cfg(test)]
mod day202210 {
    use crate::day202210::day202210::Instruction::Noop;
    use crate::file::{load_and_run_result, Res};

    #[derive(Debug)]
    struct Ctx {
        instructions: Vec<Instruction>,
        // What is the current cycle
        cycle: i64,
        // What is the current instruction?
        offset: i64,
        // What is the current offset inside the instruction?
        sub_offset: i64,
        x: i64,
        screen: Vec<bool>,
    }

    #[derive(Debug, Copy, Clone)]
    enum Instruction {
        Noop,
        Addx(i64),
    }

    const SCREEN_WIDTH: i64 = 40;
    const SCREEN_HEIGHT: i64 = 6;

    impl Ctx {
        fn parse(x: &str) -> Res<Ctx> {
            let mut instructions = vec![];
            for line in x.lines() {
                let parts: Vec<&str> = line.split(" ").collect();
                if parts.len() < 1 {
                    continue;
                }
                if parts.len() == 1 && parts[0] == "noop" {
                    instructions.push(Noop);
                } else if parts.len() == 2 && parts[0] == "addx" {
                    instructions.push(Instruction::Addx(parts[1].parse::<i64>()?));
                } else {
                    return Err("unknown instruction".into());
                }
            }
            return Ok(Ctx {
                instructions,
                cycle: 0,
                offset: 0,
                sub_offset: 0,
                x: 1,
                screen: vec![false; (SCREEN_WIDTH * SCREEN_HEIGHT) as usize],
            });
        }

        fn step(&mut self) {
            self.cycle += 1;
            // Read the current instruction
            let inst = self.instructions[self.offset as usize].into();
            let len = match inst {
                Instruction::Addx(_) => 2,
                _ => 1,
            };
            // Check if we're still in that instruction
            self.sub_offset += 1;
            if self.sub_offset < len {
                return;
            }
            // We're done, execute and move on
            self.offset = (self.offset + 1) % self.instructions.len() as i64;
            self.sub_offset = 0;
            match inst {
                Instruction::Addx(v) => self.x += v,
                _ => {}
            }
        }

        fn run(&mut self, steps: i64) -> i64 {
            for _ in 0..steps {
                self.step();
            }
            return self.x * (self.cycle + 1);
        }

        fn run_a(&mut self) -> i64 {
            let mut sum = 0i64;
            sum += self.run(19);
            sum += self.run(40);
            sum += self.run(40);
            sum += self.run(40);
            sum += self.run(40);
            sum += self.run(40);
            return sum;
        }

        fn plot(&self) -> String {
            // ADding \n to make it easier to read in my result output
            let mut s: String = "\n".into();
            for y in 0..SCREEN_HEIGHT {
                for x in 0..SCREEN_WIDTH {
                    let v = self.screen[(x + y * SCREEN_WIDTH) as usize];
                    s.push(if v { '#' } else { '.' });
                }
                s.push('\n')
            }
            return s;
        }

        fn run_b(&mut self) -> String {
            for i in 0..SCREEN_WIDTH * SCREEN_HEIGHT {
                // Paint
                let x = i % SCREEN_WIDTH;
                let hit_sprite = (self.x - x).abs() <= 1;
                self.screen[i as usize] = hit_sprite;
                // Execute instruction
                self.step();
            }
            return self.plot();
        }
    }

    fn solve_a(x: &str) -> Res<i64> {
        let mut ctx = Ctx::parse(x)?;
        return Ok(ctx.run_a());
    }

    fn solve_b(x: &str) -> Res<String> {
        let mut ctx = Ctx::parse(x)?;
        return Ok(ctx.run_b());
    }

    static TEST_CASE: &str = "addx 15\naddx -11\naddx 6\naddx -3\naddx 5\naddx -1\naddx -8\naddx 13\naddx 4\nnoop\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx -35\naddx 1\naddx 24\naddx -19\naddx 1\naddx 16\naddx -11\nnoop\nnoop\naddx 21\naddx -15\nnoop\nnoop\naddx -3\naddx 9\naddx 1\naddx -3\naddx 8\naddx 1\naddx 5\nnoop\nnoop\nnoop\nnoop\nnoop\naddx -36\nnoop\naddx 1\naddx 7\nnoop\nnoop\nnoop\naddx 2\naddx 6\nnoop\nnoop\nnoop\nnoop\nnoop\naddx 1\nnoop\nnoop\naddx 7\naddx 1\nnoop\naddx -13\naddx 13\naddx 7\nnoop\naddx 1\naddx -33\nnoop\nnoop\nnoop\naddx 2\nnoop\nnoop\nnoop\naddx 8\nnoop\naddx -1\naddx 2\naddx 1\nnoop\naddx 17\naddx -9\naddx 1\naddx 1\naddx -3\naddx 11\nnoop\nnoop\naddx 1\nnoop\naddx 1\nnoop\nnoop\naddx -13\naddx -19\naddx 1\naddx 3\naddx 26\naddx -30\naddx 12\naddx -1\naddx 3\naddx 1\nnoop\nnoop\nnoop\naddx -9\naddx 18\naddx 1\naddx 2\nnoop\nnoop\naddx 9\nnoop\nnoop\nnoop\naddx -1\naddx 2\naddx -37\naddx 1\naddx 3\nnoop\naddx 15\naddx -21\naddx 22\naddx -6\naddx 1\nnoop\naddx 2\naddx 1\nnoop\naddx -10\nnoop\nnoop\naddx 20\naddx 1\naddx 2\naddx 2\naddx -6\naddx -11\nnoop\nnoop\nnoop\n";

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(solve_a(TEST_CASE)?, 13140);
        load_and_run_result(10, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b(TEST_CASE)?, "\n##..##..##..##..##..##..##..##..##..##..\n###...###...###...###...###...###...###.\n####....####....####....####....####....\n#####.....#####.....#####.....#####.....\n######......######......######......####\n#######.......#######.......#######.....\n");
        load_and_run_result(10, 'b', solve_b);
        return Ok(());
    }
}
