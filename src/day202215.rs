#[cfg(test)]
mod day202215 {
    use crate::file::{load_and_run_result, Res};
    use crate::vec2d::Vec2d;
    use std::cmp::Ordering;

    #[derive(Debug)]
    struct Ctx {
        sensors: Vec<Sensor>,
    }

    #[derive(Debug)]
    struct Sensor {
        position: Vec2d,
        beacon: Vec2d,
    }

    #[derive(Debug, Eq, PartialEq, Clone)]
    struct Span {
        // Inclusive interval, so start 1 end 2 has length 2
        // start = end is valid 1 segment span
        start: i64,
        end: i64,
    }

    #[derive(Debug)]
    struct Spans {
        spans: Vec<Span>,
    }

    impl Ord for Span {
        fn cmp(&self, other: &Self) -> Ordering {
            return self.start.cmp(&other.start);
        }
    }

    impl PartialOrd for Span {
        fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
            Some(self.cmp(other))
        }
    }

    impl Spans {
        fn add(&mut self, start: i64, end: i64) {
            if end < start {
                return;
            }
            // Add the new span
            self.spans.push(Span { start, end });
            // Sort the spans by their start position
            self.spans.sort();
            // Now build a new list, combining spans if they overlap
            let mut spans: Vec<Span> = Vec::new();
            for span in self.spans.as_slice() {
                // If there is already a span, check if it overlaps
                match spans.last_mut() {
                    Some(last) => {
                        // Check if the last span overlaps with this one. If so, extend the last span
                        if last.end >= span.start {
                            // Overlap detected
                            if last.end < span.end {
                                // Not Fully contained, extend
                                last.end = span.end;
                            }
                            continue;
                        }
                    }
                    _ => {}
                };
                // This is a new span
                spans.push(span.clone());
            }
            self.spans = spans;
        }

        fn has_hole(&self, start: i64, end: i64) -> i64 {
            let mut pos = start;
            for span in self.spans.as_slice() {
                if span.start > pos {
                    // pos is the hole
                    return pos;
                }
                if span.end >= end {
                    // No hole
                    return -1;
                }
                if span.end > pos {
                    pos = span.end + 1;
                }
            }
            return -1;
        }

        fn total(&self) -> usize {
            let mut total = 0;
            for span in self.spans.as_slice() {
                let len = span.end - span.start + 1;
                total += len as usize;
            }
            return total;
        }
    }

    impl Ctx {
        fn parse(x: &str) -> Res<Ctx> {
            let mut sensors = Vec::new();
            for line in x.lines() {
                if line.len() == 0 {
                    continue;
                }
                let noprefix = line
                    .strip_prefix("Sensor at ")
                    .ok_or("invalid input".to_string())?;
                let (left, right) = noprefix
                    .split_once(": closest beacon is at ")
                    .ok_or("invalid input".to_string())?;
                let left2 = left.replace("x=", "").replace(" y=", "");
                let right2 = right.replace("x=", "").replace(" y=", "");
                let position = left2.parse::<Vec2d>()?;
                let beacon = right2.parse::<Vec2d>()?;
                sensors.push(Sensor { position, beacon });
            }
            return Ok(Ctx { sensors });
        }

        fn scan_row(&self, row: i64, skip_beacons: bool) -> Spans {
            let mut spans = Spans { spans: vec![] };
            for sensor in self.sensors.as_slice() {
                let delta = sensor.beacon.sub(&sensor.position);
                let distance = delta.l0();
                // Project to y=10
                let distance_y = (sensor.position.y - row).abs();
                if distance_y > distance {
                    // no contribution
                    continue;
                }
                let w = distance - distance_y;
                // sensor.position.x
                let mut start = sensor.position.x - w;
                let mut end = sensor.position.x + w;
                if skip_beacons && sensor.beacon.y == row {
                    if start == sensor.beacon.x {
                        start += 1;
                    }
                    if end == sensor.beacon.x {
                        end -= 1;
                    }
                }
                spans.add(start, end);
            }
            return spans;
        }

        fn solve_a(&self, row: i64) -> usize {
            return self.scan_row(row, true).total();
        }

        fn solve_b(&self, size: i64) -> usize {
            for i in 0..size {
                let spans = self.scan_row(i, false);
                let hole = spans.has_hole(0, size);
                if hole >= 0 {
                    return (hole * 4000000 + i) as usize;
                }
            }
            return 0;
        }
    }

    fn solve_a_test(x: &str) -> Res<usize> {
        let ctx = Ctx::parse(x)?;
        return Ok(ctx.solve_a(10));
    }

    fn solve_a(x: &str) -> Res<usize> {
        let ctx = Ctx::parse(x)?;
        return Ok(ctx.solve_a(2000000));
    }

    fn solve_b_test(x: &str) -> Res<usize> {
        let ctx = Ctx::parse(x)?;
        return Ok(ctx.solve_b(20));
    }

    fn solve_b(x: &str) -> Res<usize> {
        let ctx = Ctx::parse(x)?;
        return Ok(ctx.solve_b(4000000));
    }

    static TEST_CASE: &str = "Sensor at x=2, y=18: closest beacon is at x=-2, y=15\nSensor at x=9, y=16: closest beacon is at x=10, y=16\nSensor at x=13, y=2: closest beacon is at x=15, y=3\nSensor at x=12, y=14: closest beacon is at x=10, y=16\nSensor at x=10, y=20: closest beacon is at x=10, y=16\nSensor at x=14, y=17: closest beacon is at x=10, y=16\nSensor at x=8, y=7: closest beacon is at x=2, y=10\nSensor at x=2, y=0: closest beacon is at x=2, y=10\nSensor at x=0, y=11: closest beacon is at x=2, y=10\nSensor at x=20, y=14: closest beacon is at x=25, y=17\nSensor at x=17, y=20: closest beacon is at x=21, y=22\nSensor at x=16, y=7: closest beacon is at x=15, y=3\nSensor at x=14, y=3: closest beacon is at x=15, y=3\nSensor at x=20, y=1: closest beacon is at x=15, y=3\n";

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(
            solve_a_test("Sensor at x=8, y=7: closest beacon is at x=2, y=10\n")?,
            12
        );
        assert_eq!(solve_a_test(TEST_CASE)?, 26);
        load_and_run_result(15, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b_test(TEST_CASE)?, 56000011);
        load_and_run_result(15, 'b', solve_b);
        return Ok(());
    }
}
