#[cfg(test)]
mod day202201 {
    use crate::file::*;

    fn parse(x: &str) -> Vec<i64> {
        let mut totals: Vec<i64> = Vec::new();
        let mut total: i64 = 0;
        for part in x.split('\n') {
            if part.trim().len() == 0 {
                totals.push(total);
                total = 0;
                continue;
            }
            let v = part.parse::<i64>().expect("valid number");
            total += v;
        }
        totals.push(total);
        totals.sort();
        totals.reverse();
        return totals;
    }

    fn solve_a(x: &str) -> i64 {
        return parse(x)[0];
    }

    fn solve_b(x: &str) -> i64 {
        let totals = parse(x);
        return totals[0] + totals[1] + totals[2];
    }

    #[test]
    fn test_a() {
        assert_eq!(
            solve_a("1000\n2000\n3000\n\n4000\n\n5000\n6000\n\n7000\n8000\n9000\n\n10000"),
            24000
        );
        load_and_run(1, 'a', solve_a);
    }

    #[test]
    fn test_b() {
        assert_eq!(
            solve_b("1000\n2000\n3000\n\n4000\n\n5000\n6000\n\n7000\n8000\n9000\n\n10000"),
            45000
        );
        load_and_run(1, 'b', solve_b);
    }
}
