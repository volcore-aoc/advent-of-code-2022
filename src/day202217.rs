#[cfg(test)]
mod day202217 {
    use crate::file::{load_and_run_result, Res};
    use crate::vec2d::Vec2d;
    use std::cmp::max;
    use std::collections::hash_map::DefaultHasher;
    use std::collections::{HashMap, HashSet};
    use std::hash::Hash;
    use std::hash::Hasher;

    #[derive(Debug)]
    struct Ctx {
        input: String,
        step: usize,
        current_row: i64,
        current_shape: usize,
        blocked: HashSet<usize>,
    }

    struct Shape {
        points: Vec<Vec2d>,
        width: i64,
        height: i64,
    }

    #[derive(Clone)]
    struct History {
        // What was the height at which this history element happen
        height: i64,
        // Which step did this history element happen at?
        step: usize,
    }

    fn xy2idx(xy: Vec2d) -> usize {
        return (xy.x + xy.y * 7) as usize;
    }

    fn get_shape(idx: usize) -> Shape {
        return match idx % 5 {
            0 => Shape {
                points: vec![
                    Vec2d { x: 0, y: 0 },
                    Vec2d { x: 1, y: 0 },
                    Vec2d { x: 2, y: 0 },
                    Vec2d { x: 3, y: 0 },
                ],
                width: 4,
                height: 1,
            },
            1 => Shape {
                points: vec![
                    Vec2d { x: 1, y: 0 },
                    Vec2d { x: 0, y: 1 },
                    Vec2d { x: 1, y: 1 },
                    Vec2d { x: 2, y: 1 },
                    Vec2d { x: 1, y: 2 },
                ],
                width: 3,
                height: 3,
            },
            2 => Shape {
                points: vec![
                    Vec2d { x: 0, y: 0 },
                    Vec2d { x: 1, y: 0 },
                    Vec2d { x: 2, y: 0 },
                    Vec2d { x: 2, y: 1 },
                    Vec2d { x: 2, y: 2 },
                ],
                width: 3,
                height: 3,
            },
            3 => Shape {
                points: vec![
                    Vec2d { x: 0, y: 0 },
                    Vec2d { x: 0, y: 1 },
                    Vec2d { x: 0, y: 2 },
                    Vec2d { x: 0, y: 3 },
                ],
                width: 1,
                height: 4,
            },
            _ => Shape {
                points: vec![
                    Vec2d { x: 0, y: 0 },
                    Vec2d { x: 0, y: 1 },
                    Vec2d { x: 1, y: 0 },
                    Vec2d { x: 1, y: 1 },
                ],
                width: 2,
                height: 2,
            },
        };
    }

    impl Ctx {
        fn parse(x: &str) -> Res<Ctx> {
            return Ok(Ctx {
                input: x.trim().to_string(),
                step: 0,
                blocked: HashSet::new(),
                current_row: 0,
                current_shape: 0,
            });
        }

        fn is_blocked(&self, shape: &Shape, xy: Vec2d) -> bool {
            if xy.y < 0 || xy.x < 0 || xy.x + shape.width > 7 {
                return true;
            }
            for p in &shape.points {
                let pos = xy.add(p);
                if self.blocked.contains(&xy2idx(pos)) {
                    return true;
                }
            }
            return false;
        }

        fn step(&mut self) {
            // Spawn next shape
            let shape = &get_shape(self.current_shape);
            self.current_shape = (self.current_shape + 1) % 5;
            let mut xy = Vec2d {
                x: 2,
                y: self.current_row + 3,
            };
            // Move shape until convergence
            loop {
                // Read instruction
                let input = self.input.chars().nth(self.step);
                self.step = (self.step + 1) % self.input.len();
                // Try move left/right
                let shift = match input {
                    Some('<') => Vec2d { x: -1, y: 0 },
                    _ => Vec2d { x: 1, y: 0 },
                };
                if !self.is_blocked(shape, xy.add(&shift)) {
                    xy = xy.add(&shift);
                }
                // Try move down
                if self.is_blocked(shape, xy.sub(&Vec2d::Y)) {
                    // Blit and finish
                    for p in &shape.points {
                        let pos = xy.add(p);
                        self.blocked.insert(xy2idx(pos));
                    }
                    self.current_row = max(self.current_row, xy.y + shape.height);
                    return;
                }
                // can move down, continue
                xy.y -= 1;
            }
        }

        #[allow(dead_code)]
        fn dump(&self) {
            for y in (0..self.current_row + 2).rev() {
                print!("|");
                for x in 0..7 {
                    if self.blocked.contains(&xy2idx(Vec2d { x, y })) {
                        print!("#");
                    } else {
                        print!(".");
                    }
                }
                println!("|");
            }
            println!("+-------+")
        }

        fn get_hash(&self) -> u64 {
            let mut h = DefaultHasher::new();
            self.current_shape.hash(&mut h);
            self.step.hash(&mut h);
            for dy in 0..50 {
                let y = self.current_row - dy;
                for x in 0..7 {
                    let blocked = self.blocked.contains(&xy2idx(Vec2d { x, y }));
                    blocked.hash(&mut h);
                }
            }
            return h.finish();
        }

        fn run(&mut self, steps: usize) {
            let mut history: HashMap<u64, History> = HashMap::new();
            let mut step_idx: usize = 0;
            let mut skipped_rows: i64 = 0;
            while step_idx < steps {
                self.step();
                let hash = self.get_hash();
                match history.get(&hash) {
                    Some(last) => {
                        // Compute the difference in height
                        let delta_rows = self.current_row - last.height;
                        let delta_steps = step_idx - last.step;
                        // Fast forward the cycle
                        let steps_remaining = steps - step_idx;
                        let reps = steps_remaining / delta_steps;
                        if reps > 0 {
                            step_idx += reps * delta_steps;
                            skipped_rows += reps as i64 * delta_rows;
                        }
                    }
                    None => {
                        history.insert(
                            hash,
                            History {
                                step: step_idx,
                                height: self.current_row,
                            },
                        );
                    }
                }
                step_idx += 1;
            }
            // Add the skipped rows.
            self.current_row += skipped_rows;
        }
    }

    fn solve_a(x: &str) -> Res<i64> {
        let mut ctx = Ctx::parse(x)?;
        ctx.run(2022);
        return Ok(ctx.current_row);
    }

    fn solve_b(x: &str) -> Res<i64> {
        let mut ctx = Ctx::parse(x)?;
        ctx.run(1000000000000);
        return Ok(ctx.current_row);
    }

    static TEST_CASE: &str = ">>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>\n";

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(solve_a(TEST_CASE)?, 3068);
        load_and_run_result(17, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b(TEST_CASE)?, 1514285714288);
        load_and_run_result(17, 'b', solve_b);
        return Ok(());
    }
}
