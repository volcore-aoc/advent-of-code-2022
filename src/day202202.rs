#[cfg(test)]
mod day202202 {
    use crate::file::load_and_run;

    fn score_a(a: char, b: char) -> i64 {
        return match b {
            'X' => {
                1 + match a {
                    'A' => 3,
                    'B' => 0,
                    'C' => 6,
                    _ => 0,
                }
            }
            'Y' => {
                2 + match a {
                    'A' => 6,
                    'B' => 3,
                    'C' => 0,
                    _ => 0,
                }
            }
            'Z' => {
                3 + match a {
                    'A' => 0,
                    'B' => 6,
                    'C' => 3,
                    _ => 0,
                }
            }
            _ => 0,
        };
    }

    fn score_b(a: char, b: char) -> i64 {
        return match b {
            'X' => {
                // lose
                0 + match a {
                    'A' => 3, // need scissor
                    'B' => 1, // need rock
                    'C' => 2, // need paper
                    _ => 0,
                }
            }
            'Y' => {
                // draw
                3 + match a {
                    'A' => 1, // need rock
                    'B' => 2, // need paper
                    'C' => 3, // need scissor
                    _ => 0,
                }
            }
            'Z' => {
                // win
                6 + match a {
                    'A' => 2, // need paper
                    'B' => 3, // need scissor
                    'C' => 1, // need rock
                    _ => 0,
                }
            }
            _ => 0,
        };
    }

    fn parse(x: &str, f: fn(a: char, b: char) -> i64) -> i64 {
        let mut total = 0i64;
        for part in x.split('\n') {
            let a = part.chars().nth(0).unwrap_or(' ');
            let b = part.chars().nth(2).unwrap_or(' ');
            total += f(a, b);
        }
        return total;
    }

    fn solve_a(x: &str) -> i64 {
        return parse(x, score_a);
    }

    fn solve_b(x: &str) -> i64 {
        return parse(x, score_b);
    }

    #[test]
    fn test_a() {
        assert_eq!(solve_a("A Y\nB X\nC Z\n"), 15);
        load_and_run(2, 'a', solve_a);
    }

    #[test]
    fn test_b() {
        assert_eq!(solve_b("A Y\nB X\nC Z\n"), 12);
        load_and_run(2, 'b', solve_b);
    }
}
