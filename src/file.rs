use std::error::Error;
use std::fmt::Display;
use std::fs;

pub fn load(path: &str) -> String {
    return fs::read_to_string(path).expect("should succed to load file");
}

pub fn load_and_run(day: i64, challenge: char, f: fn(x: &str) -> i64) {
    let path = format!("input/2022{:02}.txt", day);
    println!(
        "day {:2}{} result  = {}",
        day,
        challenge,
        f(&load(path.as_str()))
    );
}

pub fn load_and_run_result<T: Display>(
    day: i64,
    challenge: char,
    f: fn(x: &str) -> Result<T, Box<dyn Error>>,
) {
    let path = format!("input/2022{:02}.txt", day);
    let res = f(&load(path.as_str()));
    if res.is_err() {
        println!(
            "day {:2}{} result = {}",
            day,
            challenge,
            res.err().unwrap().to_string()
        );
        return;
    }
    println!("day {:2}{} result  = {}", day, challenge, res.unwrap());
}

pub type Res<T> = Result<T, Box<dyn Error>>;
