#[cfg(test)]
mod day202205 {
    use crate::file::{load_and_run_result, Res};
    use std::collections::VecDeque;

    #[derive(Debug)]
    struct Instruction {
        count: usize,
        from: usize,
        to: usize,
    }

    #[derive(Debug)]
    struct Ctx {
        buckets: Vec<VecDeque<char>>,
        instructions: Vec<Instruction>,
    }

    impl Ctx {
        fn result(&self) -> Res<String> {
            let mut result = String::new();
            for bucket in self.buckets.as_slice() {
                let c = bucket.back();
                result.push(c.ok_or("empty bucket when compiling result")?.clone());
            }
            return Ok(result);
        }

        fn exec(&mut self, dir: bool) -> Res<()> {
            for inst in self.instructions.as_slice() {
                let mut list: VecDeque<char> = VecDeque::new();
                for _ in 0..inst.count {
                    let c = self.buckets[inst.from].pop_back();
                    let vc = c.ok_or("tried to pop from empty bucket")?;
                    if dir {
                        list.push_front(vc);
                    } else {
                        list.push_back(vc);
                    }
                }
                for c in list {
                    self.buckets[inst.to].push_back(c);
                }
            }
            return Ok(());
        }
    }

    fn parse(x: &str) -> Res<Ctx> {
        let mut buckets: Vec<VecDeque<char>> = vec![];
        let mut instructions: Vec<Instruction> = vec![];
        let mut mode = 0;
        for line in x.lines() {
            if mode == 0 {
                if buckets.len() == 0 {
                    buckets.resize(line.len() / 4 + 1, VecDeque::new());
                }
                if line.len() == 0 {
                    mode = 1;
                    continue;
                }
                for i in 0..line.len() {
                    let c = line.chars().nth(i).ok_or("len changed?")?;
                    if c < 'A' || c > 'Z' {
                        continue;
                    }
                    let bucket_idx = i / 4;
                    buckets[bucket_idx].push_front(c);
                }
            } else {
                let parts: Vec<&str> = line.split(" ").collect();
                if parts.len() != 6 {
                    continue;
                }
                instructions.push(Instruction {
                    count: parts[1].parse::<usize>()?,
                    from: parts[3].parse::<usize>()? - 1,
                    to: parts[5].parse::<usize>()? - 1,
                });
            }
        }
        return Ok(Ctx {
            buckets,
            instructions,
        });
    }

    fn solve_a(x: &str) -> Res<String> {
        let mut ctx = parse(x)?;
        ctx.exec(false)?;
        return Ok(ctx.result()?);
    }

    fn solve_b(x: &str) -> Res<String> {
        let mut ctx = parse(x)?;
        ctx.exec(true)?;
        return Ok(ctx.result()?);
    }

    static TEST_CASE: &str = "    [D]    \n[N] [C]    \n[Z] [M] [P]\n 1   2   3 \n\nmove 1 from 2 to 1\nmove 3 from 1 to 3\nmove 2 from 2 to 1\nmove 1 from 1 to 2\n";

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(solve_a(TEST_CASE)?, "CMZ".to_string());
        load_and_run_result(5, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b(TEST_CASE)?, "MCD".to_string());
        load_and_run_result(5, 'b', solve_b);
        return Ok(());
    }
}
