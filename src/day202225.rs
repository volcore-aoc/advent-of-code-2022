#[cfg(test)]
mod day202225 {
    use crate::file::{load_and_run_result, Res};

    #[derive(Debug)]
    struct Ctx {
        snafus: Vec<i128>,
    }

    fn parse_snafu(input: &str) -> Res<i128> {
        let mut res = 0i128;
        for c in input.chars() {
            // Shift result left by 5
            res *= 5;
            res += match c {
                '0' => 0,
                '1' => 1,
                '2' => 2,
                '=' => -2,
                '-' => -1,
                _ => return Err("invalid snafu".into()),
            };
        }
        return Ok(res);
    }

    fn to_snafu(v: i128) -> String {
        let mut x = v;
        let mut s: String = "".into();
        while x > 0 {
            let c = match x % 5 {
                0 => '0',
                1 => '1',
                2 => '2',
                3 => {
                    x += 2;
                    '='
                }
                _ => {
                    x += 1;
                    '-'
                }
            };
            x = x / 5;
            s.insert(0, c);
        }
        return s;
    }

    impl Ctx {
        fn parse(input: &str) -> Res<Ctx> {
            let mut snafus = Vec::new();
            for line in input.trim().lines() {
                snafus.push(parse_snafu(line)?);
            }
            return Ok(Ctx { snafus });
        }
    }

    fn solve_a(x: &str) -> Res<String> {
        let ctx = Ctx::parse(x)?;
        return Ok(to_snafu(ctx.snafus.iter().sum()));
    }

    static TEST_CASE: &str =
        "1=-0-2\n12111\n2=0=\n21\n2=01\n111\n20012\n112\n1=-1=\n1-12\n12\n1=\n122\n";

    #[test]
    fn test_a() -> Res<()> {
        for i in 1..20 {
            let s = to_snafu(i);
            let x = parse_snafu(&s)?;
            assert_eq!(x, i);
        }
        assert_eq!(parse_snafu("1")?, 1);
        assert_eq!(parse_snafu("2")?, 2);
        assert_eq!(parse_snafu("1=")?, 3);
        assert_eq!(parse_snafu("1-")?, 4);
        assert_eq!(parse_snafu("10")?, 5);
        assert_eq!(parse_snafu("11")?, 6);
        assert_eq!(parse_snafu("12")?, 7);
        assert_eq!(parse_snafu("2=")?, 8);
        assert_eq!(parse_snafu("2-")?, 9);
        assert_eq!(parse_snafu("20")?, 10);
        assert_eq!(parse_snafu("1=-0-2")?, 1747);
        assert_eq!(to_snafu(1), "1");
        assert_eq!(to_snafu(2), "2");
        assert_eq!(to_snafu(3), "1=");
        assert_eq!(to_snafu(4), "1-");
        assert_eq!(solve_a(TEST_CASE)?, to_snafu(4890));
        load_and_run_result(25, 'a', solve_a);
        return Ok(());
    }
}
