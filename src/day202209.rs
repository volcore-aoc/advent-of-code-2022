#[cfg(test)]
mod day202209 {
    use crate::file::{load_and_run_result, Res};
    use crate::vec2d::Vec2d;
    use std::collections::HashSet;

    #[derive(Debug)]
    struct Ctx {
        instructions: Vec<Instruction>,
        pos_h: Vec2d,
        knots: Vec<Vec2d>,
        visited: HashSet<Vec2d>,
    }

    #[derive(Debug, Copy, Clone)]
    struct Instruction {
        dir: Vec2d,
        dist: i64,
    }

    impl Ctx {
        fn dir_to_vec(dir: char) -> Vec2d {
            return match dir {
                'U' => Vec2d::Y,
                'D' => Vec2d::Y.neg(),
                'R' => Vec2d::X,
                'L' => Vec2d::X.neg(),
                _ => Vec2d::ZERO,
            };
        }

        fn parse(x: &str) -> Res<Ctx> {
            let mut instructions = vec![];
            for line in x.lines() {
                if line.len() == 0 {
                    continue;
                }
                let dir = line.chars().nth(0).ok_or("invalid instruction")?;
                let dist = *&line[2..].parse::<i64>()?;
                instructions.push(Instruction {
                    dir: Ctx::dir_to_vec(dir),
                    dist,
                })
            }
            return Ok(Ctx {
                instructions,
                pos_h: Vec2d::ZERO,
                knots: Vec::new(),
                visited: HashSet::new(),
            });
        }

        fn follow(h: &Vec2d, t: &mut Vec2d) {
            let diff = h.sub(t);
            if diff.l0() > 2 {
                // Move diagonally
                t.x += diff.x.signum();
                t.y += diff.y.signum();
            } else if diff.l0() == 2 {
                // Might be diagonal, in which case it's still fine
                if diff.x != 0 && diff.y != 0 {
                    return;
                }
                if diff.x == 0 {
                    t.y += diff.y.signum()
                } else {
                    t.x += diff.x.signum()
                }
            } else {
                return;
            }
        }

        fn mov(&mut self, inst: &Instruction) -> Res<()> {
            for _ in 0..inst.dist {
                self.pos_h = self.pos_h.add(&inst.dir);
                let mut head = self.pos_h;
                for knot in &mut self.knots {
                    Ctx::follow(&head, knot);
                    head = *knot;
                }
                let last = *self.knots.last().ok_or("no last knot")?;
                self.visited.insert(last);
            }
            return Ok(());
        }

        fn run(&mut self, count: usize) -> Res<()> {
            let instructions = self.instructions.to_vec();
            self.knots = vec![Vec2d::ZERO; count];
            for inst in instructions.as_slice() {
                self.mov(inst)?;
            }
            return Ok(());
        }

        fn count(&self) -> i64 {
            return self.visited.len() as i64;
        }
    }

    fn solve_a(x: &str) -> Res<i64> {
        let mut ctx = Ctx::parse(x)?;
        ctx.run(1)?;
        return Ok(ctx.count());
    }

    fn solve_b(x: &str) -> Res<i64> {
        let mut ctx = Ctx::parse(x)?;
        ctx.run(9)?;
        return Ok(ctx.count());
    }

    static TEST_CASE: &str = "R 4\nU 4\nL 3\nD 1\nR 4\nD 1\nL 5\nR 2\n";
    static TEST_CASE2: &str = "R 5\nU 8\nL 8\nD 3\nR 17\nD 10\nL 25\nU 20\n";

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(solve_a(TEST_CASE)?, 13);
        load_and_run_result(9, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b(TEST_CASE)?, 1);
        assert_eq!(solve_b(TEST_CASE2)?, 36);
        load_and_run_result(9, 'b', solve_b);
        return Ok(());
    }
}
