#[cfg(test)]
mod day202216 {
    use crate::file::{load_and_run_result, Res};
    use std::cmp::max;
    use std::collections::{HashMap, HashSet, VecDeque};

    #[derive(Debug)]
    struct Ctx {
        rooms: HashMap<String, Room>,
        targets: Vec<String>,
        paths: HashMap<String, usize>,
    }

    #[derive(Debug, Clone)]
    struct Room {
        valve: usize,
        tunnels: Vec<String>,
        links: HashMap<String, usize>,
    }

    #[derive(Debug, Clone)]
    struct PathBuilder {
        path: String,
        total: usize,
        flow: usize,
        time_left: i64,
        visited: HashSet<String>,
        current: String,
    }

    impl Ctx {
        fn parse(x: &str) -> Res<Ctx> {
            let mut rooms = HashMap::new();
            let mut targets = Vec::new();
            for line in x.lines() {
                if line.len() == 0 {
                    continue;
                }
                let mut line2 = line.replace("Valve ", "");
                line2 = line2.replace(" has flow rate=", ";");
                line2 = line2.replace(" tunnel leads to valve ", "");
                line2 = line2.replace(" tunnels lead to valves ", "");
                let parts: Vec<&str> = line2.split(";").collect();
                if parts.len() != 3 {
                    return Err("invalid line".into());
                }
                let rate = parts[1].parse::<usize>()?;
                let name = parts[0];
                let tunnels = parts[2].split(", ").map(|x| x.to_string()).collect();
                rooms.insert(
                    parts[0].into(),
                    Room {
                        tunnels,
                        valve: rate,
                        links: HashMap::new(),
                    },
                );
                if rate > 0 {
                    targets.push(name.into());
                }
            }
            return Ok(Ctx {
                rooms,
                targets,
                paths: HashMap::new(),
            });
        }

        fn build_node(&mut self, source: &str) -> Res<()> {
            let mut visited: HashSet<String> = HashSet::new();
            // We don't need a priority queue as we always add 1 to the distance to every
            // neighbour. So there is no re-ordering needed
            let mut queue: VecDeque<(String, usize)> = VecDeque::new();
            visited.insert(source.to_string());
            queue.push_back((source.to_string(), 0));
            while let Some(next) = queue.pop_front() {
                let room: Room = self.rooms.get(&next.0).ok_or("err".to_string())?.clone();
                // Add link to room if it's a target and we didn't have it mapped yet
                if room.valve > 0 {
                    self.rooms.entry(source.to_string()).and_modify(|x| {
                        if !x.links.contains_key(&next.0) {
                            x.links.insert(next.0.to_string(), next.1);
                        }
                    });
                }
                // Look at all the neighbours of this room
                for nb in room.tunnels.as_slice() {
                    if visited.contains(nb) {
                        continue;
                    }
                    queue.push_back((nb.to_string(), next.1 + 1));
                    visited.insert(nb.to_string());
                }
            }
            return Ok(());
        }

        fn build_graph(&mut self) -> Res<()> {
            for source in self.targets.clone().as_slice() {
                self.build_node(source)?;
            }
            // Also build for "AA", as while it's not a target, it's the origin
            self.build_node("AA")?;
            return Ok(());
        }

        fn build_path(&mut self, path: PathBuilder) -> Res<()> {
            // Check if this is an invalid path
            if path.time_left < 0 {
                return Ok(());
            }
            // Add current path to paths (assuming we don't move anymore)
            let max_value = path.total + path.flow * path.time_left as usize;
            self.paths.insert(path.path.clone(), max_value);
            // Now look at all neighbours
            for target in self.targets.clone().as_slice() {
                // Don't go there if we have visited it
                if path.visited.contains(target) {
                    continue;
                }
                // Visit it. distance containst the time it takes to turn the valve on
                let distance = self.rooms[&path.current].links[target] + 1;
                if path.time_left < distance as i64 {
                    // No point in moving here
                    continue;
                }
                let amount = self.rooms[target].valve;
                let mut new_visited = path.visited.clone();
                new_visited.insert(target.clone());
                let new_path = if path.path.len() == 0 {
                    target.clone()
                } else {
                    path.path.clone() + "," + target
                };
                self.build_path(PathBuilder {
                    path: new_path,
                    visited: new_visited,
                    flow: path.flow + amount,
                    total: path.total + path.flow * distance,
                    time_left: path.time_left - distance as i64,
                    current: target.to_string(),
                })?;
            }

            return Ok(());
        }

        fn build_paths(&mut self, time: i64) -> Res<()> {
            return self.build_path(PathBuilder {
                visited: HashSet::new(),
                path: "".into(),
                current: "AA".into(),
                time_left: time,
                total: 0,
                flow: 0,
            });
        }
    }

    fn solve_a(x: &str) -> Res<usize> {
        let mut ctx = Ctx::parse(x)?;
        ctx.build_graph()?;
        ctx.build_paths(30)?;
        // Find highest scoring path
        let mut max_score = 0;
        for (_, score) in ctx.paths {
            max_score = max(score, max_score);
        }
        return Ok(max_score);
    }

    fn solve_b(x: &str) -> Res<usize> {
        let mut ctx = Ctx::parse(x)?;
        ctx.build_graph()?;
        ctx.build_paths(26)?;
        // Find the combination of the two highest scoring paths
        // FIXME(VS): make faster by pre-sorting paths by value, and aborting if value is too low to
        //            beat max
        let mut max_score = 0;
        for (path1, score1) in ctx.paths.iter() {
            let s1: HashSet<&str> = HashSet::from_iter(path1.split(","));
            for (path2, score2) in ctx.paths.iter() {
                if score1 + score2 <= max_score {
                    continue;
                }
                let s2: HashSet<&str> = HashSet::from_iter(path2.split(","));
                if s1.intersection(&s2).count() > 0 {
                    continue;
                }
                // Found a new best
                max_score = score1 + score2;
            }
        }
        return Ok(max_score);
    }

    static TEST_CASE: &str = "Valve AA has flow rate=0; tunnels lead to valves DD, II, BB\nValve BB has flow rate=13; tunnels lead to valves CC, AA\nValve CC has flow rate=2; tunnels lead to valves DD, BB\nValve DD has flow rate=20; tunnels lead to valves CC, AA, EE\nValve EE has flow rate=3; tunnels lead to valves FF, DD\nValve FF has flow rate=0; tunnels lead to valves EE, GG\nValve GG has flow rate=0; tunnels lead to valves FF, HH\nValve HH has flow rate=22; tunnel leads to valve GG\nValve II has flow rate=0; tunnels lead to valves AA, JJ\nValve JJ has flow rate=21; tunnel leads to valve II\n";

    #[test]
    fn test_a() -> Res<()> {
        assert_eq!(solve_a(TEST_CASE)?, 1651);
        load_and_run_result(16, 'a', solve_a);
        return Ok(());
    }

    #[test]
    fn test_b() -> Res<()> {
        assert_eq!(solve_b(TEST_CASE)?, 1707);
        load_and_run_result(16, 'b', solve_b);
        return Ok(());
    }
}
